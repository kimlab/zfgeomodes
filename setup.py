from setuptools import setup, find_packages


requirements = [
            'pandas',
            'numpy',
            'scipy',
            'pytest',
            'scikit-learn',
            'tqdm',
            'python-Levenshtein',
            'matplotlib',
            'keras',
            'logomaker',
            'joblib']



setup(
    name='zfpy',
    version='0.1',
    packages=  find_packages(),
  package_data={
                'zfpy.model':['pretrained_models/*.*'],
                'zfpy.datasets':['data/*.csv'],
                'zfpy':['motifs/*.pkl'],
},

    author='kimlab.org',
    author_email='carles.corbi@kimlab.org',
    url = "http://kimlab.org",
    description = ("Code to make Zinc Fingers DNA motif predictions, generate your own model, and  analyse B1H selections data"),
    install_requires=requirements,

    classifiers=[
       "License :: OSI Approved :: MIT License",
       "Programming Language :: Python :: 3",
       "Topic :: Scientific/Engineering :: Bio-Informatics",
       ],
    license='MIT',

    entry_points={
        'console_scripts': [
                'zfpy = zfpy.zfpred:main',
        ]
    }




)
