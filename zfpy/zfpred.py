#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

import matplotlib.pyplot as plt
import logomaker

from zfpy.base import *
import zfpy.model as zfmodel
import zfpy.utils.fasta as zffasta
from zfpy.motifs import vec2df, two_bits, norm_output


def _extract_mode_nine(s):
    
    ct = re.findall(r'HH.{3,5}H.*$', s)
    if ct:
        return ct[0][3]

    ct = re.findall(r'H.{3,5}H.*$', s)
    if ct:
        return ct[0][2]

    # no canoncial
    ct = re.findall(r'H.{2,5}H.*$', s)

    if ct:
        return ct[0][2]

    return '_'

def _extract_specaminoacids(s):
    specres = list()

    core = re.findall(r'C.{1,5}C.{3}[LIVMFYWC].{8}H..', s)
    if core:
        #canonical

        for i in [-10,-9,-8,-7,-5,-4]:
            specres.append(core[0][i])
        mplus2 = core[0][-11]
        return ''.join(specres), mplus2, 'CANONICAL'
     
    core = re.findall(r'C.{1,5}C.{12}H..', s)
    if core:
#         #canonical

         for i in [-10,-9,-8,-7,-5,-4]:
             specres.append(core[0][i])
         mplus2 = core[0][-11]
         return ''.join(specres), mplus2, 'canonical' 




    return ''.join(['X','X','X','X','X','X']), 'X', 'NOTFOUND'


def parse_fasta(seq, fasta_id):
    
    results = list()
    m0 = '_'
    prev = 0
    array = 1
    for idx, core in enumerate(re.finditer(r'C.{1,5}C.{12}H.{2,5}H', seq)):
        Pfam_froms = core.start() #tf['Pfam_froms'].split(',')
        Pfam_tos = core.end() # tf['Pfam_tos'].split(',')
        DBD_seqs = core[0] #core.tf['DBD_seqs'].split(',')
        temp = list()
        specres, m1, ztype = _extract_specaminoacids(DBD_seqs)
        
        if ztype == 'NOTFOUND':
            continue

        temp.append(fasta_id)
        temp.append('F{}'.format(idx+1))

        
        if prev == 0:

            temp.append(array)
            temp.append(m0+m1)
            temp.append(specres)
            coreH = specres[0] + specres[2:4] + specres[-1]
            temp.append(coreH)
            temp.append(DBD_seqs)
            temp.append(ztype)
            
            m0 = _extract_mode_nine(DBD_seqs)
            prev = int(Pfam_tos)



        elif int(Pfam_froms)-prev >8:

            array = array + 1

            temp.append(array)
            m0 = '_'
            temp.append(m0+m1)
            temp.append(specres)
            coreH = specres[0] + specres[2:4] + specres[-1]
            temp.append(coreH)
            temp.append(DBD_seqs)
            temp.append(ztype)
            
            m0 = _extract_mode_nine(DBD_seqs)
            prev = int(Pfam_tos)



        else:

            #array =
            temp.append(array)
            temp.append(m0+m1)
            temp.append(specres)
            coreH = specres[0] + specres[2:4] + specres[-1]
            temp.append(coreH)


            temp.append(DBD_seqs)
            temp.append(ztype)
            
            m0 = _extract_mode_nine(DBD_seqs)
            prev = int(Pfam_tos)

        results.append(temp)
    

    return pd.DataFrame(results, columns=['TF_ID',  'Finger', 'Array', 'BP', 'CoreHelix', 'Core','helix_type', 'raw_seq' ])



def get_options():
    """Get arguments from command line.

    Parameters
    ----------

    Returns
    -------

    """
    parser = argparse.ArgumentParser(description="""
    From a Fasta file, find ZincFingers and generate a prediction of the DNA binding motif
    """)


    parser.add_argument('-i', '--input_fasta', action="store",
                        dest="input_fasta", default=False, help='input_fasta \
                        FASTA file. Fasta file can contain more than one sequence', required=True)



    parser.add_argument('-o', '--out_dir', action="store", dest="out_dir",
                        default=None, help='Output folder, called \
                        as the fasta file by default')

    # optional Arguments

    parser.add_argument('--model', action="store",
                        dest="model_engine", default=None,
                        help='Max number of misreading allowed in the constant \
                        constant_region (default 2)')


    parser.add_argument('--no-logo', help='Do not generate a logo', dest='logo',
                        default=True, action='store_false')


    options = parser.parse_args()

    return options

def find_zf_arrays(input_fasta):


    gs_seqs = zffasta.read_to(input_fasta, to='dict')
    tfgs_df = list()
    for idx, seq in gs_seqs.items():
            tfactor = parse_fasta(seq, idx)

            tfgs_df.append(tfactor)
            
    tfgs_df = pd.concat(tfgs_df)

    return tfgs_df



def plot_logo(array_prediction, out_dir, tfid, arrayid):

    

    fig, ax = plt.subplots(1,2, figsize=(10,3), sharey=True, sharex=True)

    forlogo_predf = two_bits(vec2df(array_prediction))    
    logomaker.Logo(forlogo_predf.astype(float),ax=ax[0], )#allow_nans=True) 
    ax[0].set_title('F {} {} '.format( tfid, arrayid))

    forlogo_predr = two_bits(vec2df(array_prediction[::-1]))
    logomaker.Logo(forlogo_predr.astype(float),ax=ax[1], )#allow_nans=True) 
    ax[1].set_title('R {} {}'.format( tfid, arrayid))


    
    ax[1].set_ylim(0,2)
    ax[1].set_yticks([0,1,2])
    ax[1].set_xticks([],[])
            

    fig.tight_layout()
    fig.savefig('./{}/{}_{}.pdf'.format(out_dir, tfid, arrayid))

def main():
    """Main Pipeline.

    Parameters
    ----------
    opts


    """

    # Read arguments
    opts = get_options()

    
    if opts.model_engine:

        oracle = zfmodel.load_model(opts.model_engine)
    else:
        # temporal hardcode default model
        default_model = './pretrained_models/201912Synthetic_cnn_Query.h5'
        abs_file_path = os.path.join(zfmodel.models_dir, default_model)
        oracle = zfmodel.load_model(abs_file_path)

    project_name = os.path.basename(opts.input_fasta).rpartition('.')[0]

    os.makedirs(project_name, exist_ok=True)

    zf_arrays = find_zf_arrays(opts.input_fasta)
    zf_arrays.to_csv('{}/ZF_arrays.csv'.format(project_name), index=False)
        
    for idj, array in zf_arrays.groupby('Array', as_index=False):
        array_prediction = list()
        TFid = array['TF_ID'].values[0]
        Arrayid = array['Array'].values[0]                
        for idi , finger in array[::-1].iterrows():
            query = finger['BP'] + finger['CoreHelix']
            query = onehot_seq(query)
            pred = oracle.predict(np.reshape(query, (1,8,21)))
            pred = norm_output(pred)
            array_prediction.extend(pred)
        array_prediction = np.asarray(array_prediction).flatten()
        motif_pred = vec2df(array_prediction)
        motif_pred.to_csv('{}/pwm_{}_{}.txt'.format(project_name, TFid, Arrayid), index=False, sep='\t', float_format='%.4f')

        if opts.logo:
            plot_logo(array_prediction, project_name, TFid, Arrayid)

if __name__ == '__main__':
    main()
