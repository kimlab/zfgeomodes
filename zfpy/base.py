import sys
import json
import os
import pickle
import random
import math
import re
from collections import defaultdict
import json
import glob
import matplotlib

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import seaborn as sns

# Modes definitions
modes_dict = {'RA':'RA','RS':'RS', 'MODE2':'RR', 'MODE3':'VS','MODE4':'KS','MODE5':'IN','MODE6':'KR'}

functional_bp = ('AR','AS','AT','FN','FR','FS','GR','HA','HN','HR','HS','HT','IN','IR','IS','IT','KR','KS','KT','LN','LR','LS','LT','MN','MR','MS','MT','NK','NN','NR','NS','NT','PN','QR','QS','QT','RA','RC','RG','RK','RN','RR','RS','RT','SH','SR','SS','ST','TA','TH','TN','TR','TS','TT','VN','VR','VS','VT','WR','YN','YR','YS')

low_functional_bp = ('AA','AH','AK','AN','AP','CR','CS','DR','DS','FA','FK','FT','GS','GT','HG','HK','IA','IH','II','IK','IY','KA','KG','KH','KK','KL','KM','KP','KQ','LA','LG','LH','LK','LL','LP','MH','MK','MP','MQ','NA','NG','NL','PR','PS','PT','QA','QG','QH','QK','QL','QN','QP','QQ','RH','RI','RL','RM','RP','RQ','RV','RY','SA','SF','SG','SK','SL','SN','SP','TG','TK','TL','TP','TQ','VA','VH','VK','WA','WK','WN','WS','WT','YA','YG','YK','YT',)


modes_expand_bp = {'MODE1':['RA','RS','RC'],
                     'MODE2':['RR', 'WR'],
                     'MODE3':['VS','YS'],
                     'MODE4':['KS', 'KT', 'NS'],
                     'MODE5':['IN', 'VN', 'IT'],
                     'MODE6':['KR', 'QR']}


# 3 bp target defitnions

triplets = list()
bases = ['A','C' ,'G', 'T']

for b1 in bases:
    for b2 in bases:
        for b3 in bases:
            triplets.append(b1+b2+b3)



# One-hot encoding AA
all_amino = ['_','C', 'V', 'T', 'F', 'Y', 'A', 'P', 'W', 'I', 'M', 'L', 'S', 'G', 'H', 'D', 'E', 'N', 'Q', 'R', 'K']
n_amino = len(all_amino)
aa_to_int = dict((c, i) for i, c in enumerate(all_amino))

int_to_aa = dict((c, i) for i, c in aa_to_int.items())



aa_one_hot = dict()
for a,i in aa_to_int.items():
    v = np.zeros(len(aa_to_int))
    v[i] = 1
    aa_one_hot[a] = v


# One-hot encoding Nucleotides


n_bases = len(bases)
base_to_int = dict((c, i) for i, c in enumerate(bases))
int_to_base = dict((c, i) for i, c in base_to_int.items())



base_one_hot = dict()
for a,i in base_to_int.items():
    v = np.zeros(len(base_to_int))
    v[i] = 1
    base_one_hot[a] = v
    

# Functions


def onehot_seq(seq, kind = 'aa'):
     if kind == 'aa':
         one_hot = aa_one_hot
     else:
         one_hot = base_one_hot

     _one_hot_seq =  list()
     for s in seq:
         _one_hot_seq.append(one_hot[s])

     return np.asarray(_one_hot_seq)


    
def decode_onehot_seq(seq, kind = 'aa'):
     if kind == 'aa':
         one_hot = int_to_aa
     else:
         one_hot = int_to_base

     _one_hot_seq =  list()
     for s in seq:
         _one_hot_seq.append(one_hot[np.argmax(s)])

     return ''.join(_one_hot_seq)


features = [ 'A1', 'C1', 'G1', 'T1', 'A2',
       'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3' ]

