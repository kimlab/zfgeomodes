import sys
import glob
import pandas as pd
import numpy as np

from ngskit import analysis
from ngskit.utils import fastq_tools
from ngskit.utils import dna

triplets = list()
bases = ['A','C' ,'G', 'T']
mode = sys.argv[1]
for b1 in bases:
    for b2 in bases:
        for b3 in bases:
            triplets.append(b1+b2+b3)

raw = list()
for t in triplets:#folder = sys.argv[3]
        fn = '{}*{}*R1*fastq.gz'
        folders = fn
        fastqs = glob.glob(folders.format(mode, t))
        tdata = list()
        for fq in fastqs:
            a = fastq_tools.read_fastq(fq)
            raw.append(len(a))


print('COUNTER {}  {} '.format(mode,sum(raw)))

