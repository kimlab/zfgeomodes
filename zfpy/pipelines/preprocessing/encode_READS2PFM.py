import sys
import glob
import pandas as pd
import numpy as np
import re
import pickle

from ngskit.analysis import *
from sklearn.utils import shuffle


all_amino = ['_','C', 'V', 'T', 'F', 'Y', 'A', 'P', 'W', 'I', 'M', 'L', 'S', 'G', 'H', 'D', 'E', 'N', 'Q', 'R', 'K']
n_amino = len(all_amino)
aa_to_int = dict((c, i) for i, c in enumerate(all_amino))

int_to_aa = dict((c, i) for i, c in aa_to_int.items())



aa_one_hot = dict()
for a,i in aa_to_int.items():
    v = np.zeros(len(aa_to_int))
    v[i] = 1
    aa_one_hot[a] = v

triplets = list()
bases = ['A','C' ,'G', 'T']

for b1 in bases:
    for b2 in bases:
        for b3 in bases:
            triplets.append(b1+b2+b3)

n_bases = len(bases)

base_to_int = dict((c, i) for i, c in enumerate(bases))
int_to_base = dict((c, i) for i, c in base_to_int.items())



base_one_hot = dict()
for a,i in base_to_int.items():
    v = np.zeros(len(base_to_int))
    v[i] = 1
    base_one_hot[a] = v

# Vecotirize
# scale
norm_props = dict()

for prop, data in aa_property_tables.items():
    minprop = min(data.values())
    maxprop = max(data.values())
    c = maxprop - minprop
    norm = dict()
    for aa, value in data.items():
        norm[aa] = (value - minprop) / c
    norm_props[prop] = norm



def extract_spec_res_marcus(zfinger):
    parts = list()
    if len(zfinger) !=0:
        for i in [8,9,10,11,13,14]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None


def marcusdf2pfm(Seq, df, ignore_if = None):
    temp = df[df['Seq']==Seq]
    buffer = list()
    #return temp
    for triplet in triplets.keys():
        if triplet in temp['Triplet'].values:
            amount = a['Reads'][a['Triplet']==triplet].values[0]
            if ignore_if:
                if ignore_if > amount:
                    continue

            for x in range(int(amount)):
                buffer.append(triplet)

    pf = get_ppm(buffer,pseudocounts=0, elements=bases)
    return pf

def onehot_seq(seq, kind = 'aa'):
    if kind == 'aa':
        one_hot = aa_one_hot
    else:
        one_hot = base_one_hot

    _one_hot_seq =  list()
    for s in seq:
        _one_hot_seq.append(one_hot[s])

    return np.asarray(_one_hot_seq)



def revertoseq(vec, kind='aa'):

    if kind == 'aa':
        int_to_char = int_to_aa
    else:
        int_to_char = int_to_base

    seq = list()
    for i in vec:
        c = list(i > 0)
        seq.append(int_to_char[c.index(True)])
    return ''.join(seq)




def embeedings(seq):

    vec = np.zeros((len(seq),6))

    for idx,a in enumerate(seq):
        vec[idx][0] = norm_props['charge'].get(a, 0.0)
        vec[idx][1] = norm_props['polarity'].get(a, 0.0)
        vec[idx][2] = norm_props['hydrophobicity'].get(a, 0.0)
        vec[idx][3] = norm_props['volume'].get(a, 0.0)
        vec[idx][4] = norm_props['local_flexibility'].get(a, 0.0)
        vec[idx][5] = norm_props['pK'].get(a, 0.0)
    return vec

def get_decoder():
    decoder = dict()
    for a in norm_props['charge'].keys():
        indx = list()
        for p in ['charge','polarity', 'hydrophobicity', 'volume', 'local_flexibility','pK']:
            indx.append(norm_props[p].get(a, 0.0))
        decoder[set(indx)]= a

    return decoder


def encode_data(df, tabu_seqs):
    dataX = list()
    dataY = list()
    for idx, row in df.iterrows():

        seq_out = row['Seq'].replace('X','A')
        #reprocess = extract_motifs(seq_out)
        #seq_out = ''.join(reprocess)
        try:
            #zmotif = itersplit_motif(seq_out)
            seq_out = extract_spec_res_marcus(seq_out)
            if seq_out in tabu_seqs:
                continue
            else:
                seq_out = seq_out.replace('X','A')
        except:
            continue
        query = onehot_seq(seq_out, kind = 'aa')
        #while len(query) < 29:
        #    query.append(0)

        query = np.array(query)
        query = np.reshape(query,(6,21,1))
        #target = [to_one_hot[char] for char in seq_out]
        features = [ 'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3'] #'A4', 'C4', 'G4', 'T4',]

        target = row[features]

        target = np.array(target)
        #target = __convertscore2freq(target)
        #dataX.append(np.reshape(result, (1, result.shape[0], result.shape[1])))
        #dataY.append(np.array(to_one_hot[seq_out]))
        dataX.append(query)
        dataY.append(target)

    X =  np.asarray(dataX_Tim)
    y = np.asarray(dataY_Tim)

    X, y = shuffle(X,y)

    return X, y



#s = "DICGRKFRSGSALWHHTKIHLRQKD"
# function to transform reads to pfm
# no pseudo counts allowed
def calc_ppm(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['Reads'].values[0]
            for i in range(int(rep)):
                buffer.append(t)
    freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)
    return list(freqs.values.flatten(order='F'))

def calc_ppm_log_flat(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['Reads'].values[0]
            rep = int(np.log10(rep))
            for i in range(int(rep)):
                buffer.append(t)
    freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)
    return list(freqs.values.flatten(order='F'))


def calc_ppm_flat(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['Reads'].values[0]
            rep = 1
            for i in range(int(rep)):
                buffer.append(t)
    freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)
    return list(freqs.values.flatten(order='F'))


def calc_ppm_usingnormreads(s, ddf):
    df = ddf[ddf['Seq']==s]
    buffer = list()
    for t in triplets:
        if t in df['Triplet'].values:
            rep = df[(df['Triplet']==t)]['nReads'].values[0]
            for i in range(int(round(rep))):
                buffer.append(t)
    try:
        freqs = get_ppm(buffer,elements=bases,pseudocounts=0.0)

    except Exception as e:
        print(rep)
        print('s')
        raise e

    return list(freqs.values.flatten(order='F'))

def loground(val):
    return round(np.log2(val))



def main():

    # put all triplets togheter
    collector = list()
    # Mode    
    m = sys.argv[1]
    # Filter Reads
    READS_CUTOFF = int(sys.argv[2])
    # Filter Entropy
    E_CUTOFF = float(sys.argv[3])
    # Motif derivation method, default reads
    # log, flat, normreads and reads
    FRQ = 'reads'

    # Load data
    for t in  triplets:
        try:
            df = pd.read_csv(f'../Data_raw/preprocess_complete/{m}_{t}_preprocess.csv')
            df['Triplet'] = t
            collector.append(df)
        except:
            print('Something went wrong for {}'.format(t))
            pass


    mergeddf = pd.concat(collector)
    print('#{} RAW SEQ in {}'.format(mergeddf.shape[0], m))

    # Motif derivation function selection
    if sys.argv[4]:
        if sys.argv[4].strip() == 'log':
            print('log reads')
            FRQ = 'logreads'
            calc_ppm_fn = calc_ppm_log_flat
        elif sys.argv[4].strip() == 'flat':
            print('flat reads')
            FRQ = 'flatreads'
            calc_ppm_fn = calc_ppm_flat
        elif sys.argv[4].strip() == 'normreads':
            print('normreads')
            FRQ = 'normreads'
            calc_ppm_fn = calc_ppm_usingnormreads

        else:
            print('reads')
            calc_ppm_fn = calc_ppm
    else:
        calc_ppm_fn = calc_ppm


    mode_aa_map = {'RS':'S','RA':'A','MODE2':'R', 'MODE3':'S', 'MODE4':'S', 'MODE5':'N', 'MODE6':'R'}
    mode_aa = mode_aa_map[m]


    # Apply filters
    # this norm is to  remove bias in deep of the seq, and reduce the weight of outliers
    mergeddf['nReads'] =  np.log2(mergeddf['Reads']/mergeddf['Reads'].sum()*1_000_000)

    # remove mutants

    mergeddf = mergeddf[mergeddf['Seq'].str.contains(r'DICGRKF.....L..HTKIHLRQKD')]
    print('#{} Filter1 SEQ in {}'.format(mergeddf.shape[0], m))

    # filter 2
    mergeddf = mergeddf[mergeddf['Reads']>READS_CUTOFF]
    #mergeddf['LReads'] = mergeddf['Reads'].apply(loground)
    print('#{} Filter2 SEQ in {}'.format(mergeddf.shape[0], m))

    #filter 3
    Q_CUTOFF = mergeddf['E'].quantile(E_CUTOFF)
    mergeddf = mergeddf[mergeddf['E']>=Q_CUTOFF]

    print('#{} Filter3 SEQ in {}'.format(mergeddf.shape[0], m))


    seq_poll = mergeddf['Seq'].unique()
    print('#{} SEQs in {} to parse'.format(seq_poll.shape[0], m))
    # transform and generate a new dataframe
    # Seq  'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3'
    totals = list()
    for s in seq_poll:
        ppm = calc_ppm_fn(s, mergeddf)
        totals.append([s]+ppm)
    # Pandify
    fet= [ 'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3']
    na = ['Seq'] + fet
    totals = pd.DataFrame(totals, columns=na)
    print('#{} SEQs in {} parsed'.format(totals.shape[0], m))

    arra = np.random.random(totals.shape[0])
    train = arra <.8
    test = arra>=.8
    FRQ = FRQ + 'NL'

    df = totals.copy()
    df.to_csv('{}_totalds_{}_{}_{}.csv'.format(m, FRQ,READS_CUTOFF,E_CUTOFF), index=False)
    X, y = encode_data(df, tabu)
    print('#{} SEQs in {} encoded'.format(X.shape[0], m))

    pickle.dump(X, open('{}_Xtotal_{}_{}_{}.pkl'.format(m,FRQ, READS_CUTOFF,E_CUTOFF),'wb'))
    pickle.dump(y, open('{}_ytotal_{}_{}_{}.pkl'.format(m, FRQ,READS_CUTOFF,E_CUTOFF),'wb'))



    df = totals[train]
    df.to_csv('{}_trainingset_{}_{}_{}.csv'.format(m,FRQ, READS_CUTOFF,E_CUTOFF), index=False)
    X, y = encode_data(df, tabu)
    print('#{} SEQs in {} encoded Traning Dataset'.format(X.shape[0], m))

    pickle.dump(X, open('{}_Xtrain_{}_{}_{}.pkl'.format(m, FRQ,READS_CUTOFF,E_CUTOFF),'wb'))
    pickle.dump(y, open('{}_ytrain_{}_{}_{}.pkl'.format(m, FRQ,READS_CUTOFF,E_CUTOFF),'wb'))


    df = totals[test]
    df.to_csv('{}_testset_{}_{}_{}.csv'.format(m, FRQ,READS_CUTOFF,E_CUTOFF), index=False)
    X, y = encode_data(df, tabu)
    pickle.dump(X, open('{}_Xtest_{}_{}_{}.pkl'.format(m,FRQ, READS_CUTOFF,E_CUTOFF),'wb'))
    pickle.dump(y, open('{}_ytest_{}_{}_{}.pkl'.format(m,FRQ, READS_CUTOFF,E_CUTOFF),'wb'))


    mergeddf['MODE_AA'] = mergeddf['Seq'].str[7]
    mergeddf = mergeddf[mergeddf['MODE_AA']==mode_aa]
    mergeddf.to_csv('{}_trainingset_classf_{}_{}_{}.csv'.format(m,FRQ, READS_CUTOFF,E_CUTOFF), index=False)



if __name__ == '__main__':
    main()


