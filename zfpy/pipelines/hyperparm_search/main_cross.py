from data import *
from model import *

# MAIN

# input variables
# mode

mode = sys.argv[1]

#stringent_level = sys.argv[3]
datap = DataPrep(read_cutoff=15, entropy_cutoff=.07, reads_fn='normreads')
#datap = DataPrep(read_cutoff=15, entropy_cutoff=0.04)
callback = [EarlyStopping(patience=10),
                ReduceLROnPlateau(patience=3, verbose=1),]

if mode == 'UNIQUE':
    slen = 8

    datap.load_unify()
    eval_corr = evaluate_simple_GSD_MP
    #
    configfile = sys.argv[2]
    log = open('LOG_{}_{}_run.out'.format( mode, configfile),'w')

    # yloss
    myloss = {"MSE":'mean_squared_error',
            'KL': 'categorical_crossentropy',
            'CORR': correlation_coefficient_loss,
            }


    configuration = json.load(open('{}.json'.format(configfile),'r'))

    l = myloss[configuration['loss_on']]


    #gmodel.load_training(mode)
    #gmodel.train(25)

    if configuration['loss_on'] == 'CORR':

        epochs = 250
    else:
        epochs = 250

    verb = 0
    batch = 512
    gsd_results = [list(),list(),list(),list()]
    folds = 1

    if 'finalAct' in configuration:
        fa = configuration['finalAct']

    else:
        fa = 'softmax'


        #datap.load_mode(mode)

    for X,y, test_XM, test_yM in datap.kf(splits=5):
            archy = Mymodel_generator(aminoacids=21, seq_len=slen, outshape=12,
                            loss=l,lr=configuration['lr'],
                            architecture= configuration['archy'],
                            optimizer=configuration['optim'],
                            finalAct=fa)

            H = archy.model.fit(X, y, epochs=epochs,
                                      verbose=verb,
                                      batch_size=batch,
                                      shuffle=True,
                                      validation_data=(test_XM,test_yM), callbacks=callback)

            e = archy.model.evaluate(test_XM,test_yM )
            print(e)
            plot_train(H,output_name='{}_{}_{}'.format(folds,mode,configfile))

            #sume = sum([e[4],e[7],e[10]])
            print('train_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)

        ##gmodel.plot(output_name=mode+'_'+configfile)





            r = eval_corr(archy.model, mode)
            flat_r = r
            print('GSD_fold_{}  {}  {}  {}  {}  {} {}'.format(folds, np.mean(flat_r),np.std(flat_r), np.median(flat_r), np.percentile(flat_r, 50),np.min(flat_r), np.max(flat_r)), file=log)
            r2 = evaluate_complex_GSD_MP(archy.model)
            flat_r2 = r2 #removenans_andflatting(r2)
            print('GSD2019_fold_{}  {}  {}  {}  {}  {} {}'.format(folds, np.mean(flat_r2),np.std(flat_r2), np.median(flat_r2), np.percentile(flat_r2, 50),np.min(flat_r2), np.max(flat_r2)), file=log)
            r3 = evaluate_complex_GSD_MP(archy.model, method='compare_motif_flanks')
            flat_r3 =  r3 #removenans_andflatting(r3)
            print('GSD2019_fl_fold_{}  {}  {}  {}  {}  {} {}'.format(folds, np.mean(flat_r3),np.std(flat_r3), np.median(flat_r3), np.percentile(flat_r3, 50),np.min(flat_r3), np.max(flat_r3)), file=log)
            folds +=1
        ##res_a.extend(ev[1])




    # mode
    plot_correlation(flat_r, flat_r2,flat_r3, output_name='{}_{}_{}'.format(folds,mode,configfile))

    #print('perf_{}  {}  '.format(o[0],o[1]), file=log)
    #r = eval_corr(archy.model, mode, normalize=True)
    #o = plot_correlation(r, output_name='norma_{}_{}_{}'.format(folds,mode,configfile))
    #print('corr_norma_{}  {}  {}  {}  {}  {}'.format(folds, np.mean(r),np.median(r),np.std(r), np.min(r), np.max(r),), file=log)
    #print('nperf_{}  {}  '.format(o[0],o[1]), file=log)
    log.close()
#plt.boxplot(cnn_toplot_mse_triplet)



