import json
import random
import hashlib
import sys
import os.path
import glob


run_script = """#!/bin/bash
#SBATCH -n 2 # number of cores
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --mem 8000 # memory pool for all cores
#SBATCH -t 0-5:05 # time (D-HH:MM)
#SBATCH --nodelist=beagle42
#SBATCH --job-name=PFM_{}{}
#SBATCH -o slurm.%N.%j.out # STDOUT
#SBATCH -e slurm.%N.%j.err # STDERR

#module load python/3.5
#module load cuda cudnn
#source $HOME/tensorflow/bin/activate

source activate keras



python main.py   UNIQUE 7d05e22c9650215e32825383a7114edd {} {}

"""


#models = glob.glob('*.json')
#a = 'UNIQUE'
for rds in ['5']:
    for en in ['0.25']:

    #name = m.replace('.json','')

        s = run_script.format(  en,rds,en,rds )
        o = open('job_setup.sh', 'w')
        print(s, file= o)
        o.close()
        #os.system('sbatch  -A def-pmkim  job_setup.sh ')
        #os.system('python main.py UNIQUE {}'.format( name))
        os.system('sbatch   job_setup.sh ')

