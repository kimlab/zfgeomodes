from data import *
from model import *

# MAIN

# input variables
# mode

mode = sys.argv[1]
en = sys.argv[3]
rds = sys.argv[4]
#stringent_level = sys.argv[3]
datap = DataPrep(folder_data='../Data/MARCUS_ZF/preprocess_complete/', read_cutoff=rds, entropy_cutoff=en, reads_fn='normreadsNL')
#datap = DataPrep(read_cutoff=15, entropy_cutoff=0.04)
callback = [EarlyStopping(patience=10),
                ReduceLROnPlateau(patience=3, verbose=1),]

if mode == 'UNIQUE':
    slen = 8
    # load dataset
    datap.load_unify()
    f2X,f2y = load_F2data()
    f3X,f3y = load_F3data()
    timx, timy = load_timdata()
    #
    configfile = sys.argv[2]
    log = open('LOG_{}_{}_{}_{}run.out'.format( mode, configfile, en, rds),'w')

    # yloss
    myloss = {"MSE":'mean_squared_error',
            'KL': 'categorical_crossentropy',
            'CORR': correlation_coefficient_loss,
            }


    configuration = json.load(open('{}.json'.format(configfile),'r'))

    l = myloss[configuration['loss_on']]


    #gmodel.load_training(mode)
    #gmodel.train(25)

    if configuration['loss_on'] == 'CORR':

        epochs = 250
    else:
        epochs = 250

    verb = 0
    batch = 512
    gsd_results = list()
    gsd_results_mse = list()
    folds = 1

    if 'finalAct' in configuration:
        fa = configuration['finalAct']

    else:
        fa = 'softmax'


        #datap.load_mode(mode)
    folds = 1
    vald_mse = list()
    hamed_mse = list()
    f2_mse = list()
    f3_mse = list()
    for X,y, test_XM, test_yM in datap.kf(splits=10):
        archy = Mymodel_generator(aminoacids=21, seq_len=slen, outshape=12,
                                loss=l,lr=configuration['lr'],
                                architecture= configuration['archy'],
                                optimizer=configuration['optim'], finalAct=fa)

        H = archy.model.fit(X, y, epochs=epochs,
                                        verbose=verb,
                                        batch_size=batch,
                                        shuffle=True,
                                        validation_data=(test_XM,test_yM),
                                        callbacks=callback)

        #archy.model.save_weights('model_weights_{}.h5'.format(folds))

        plot_train(H,output_name='{}_{}_{}'.format(folds,mode,configfile))

        e = archy.model.evaluate(timx,timy)
        print(e)
        print('Validation_Tim_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)

        e = archy.model.evaluate(f2X,f2y)
        print(e)
        print('Validation_F2_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)


        e = archy.model.evaluate(f3X,f3y)
        print(e)
        print('Validation_F3_fold_{} {} {}'.format(folds,e[1],e[2]), file=log)
            ##gmodel.plot(output_name=mode+'_'+configfile)



        r2 = evaluate_GSD(archy.model)
        # extract corr data
        flat_r2 = list(r2.values())
        print('GSDStromo_fold_{}  {}  {}  {}  {}  {} {}'.format(folds, np.nanpercentile(flat_r2, 25), np.nanmean(flat_r2), np.nanmedian(flat_r2), np.nanpercentile(flat_r2, 75),np.nanmin(flat_r2), np.nanmax(flat_r2)), file=log)
        gsd_results.append(flat_r2)
        vald_mse.append(mse_pool(test_XM,test_yM, archy.model))
        hamed_mse.append(mse_pool(timx,timy, archy.model))
        f2_mse.append(mse_pool(f2X,f2y, archy.model))
        f3_mse.append(mse_pool(f3X,f3y, archy.model))

        r3 = evaluate_GSD_MSE(archy.model)
        # extract corr data
        flat_r3 = list(r3.values())
        print('GSDStromo_MSE_{}  {}  '.format(folds,np.nanmean(flat_r3) ), file=log)
        gsd_results_mse.append(flat_r3)
            ##res_a.extend(ev[1])

        # mode
        

        #print('perf_{}  {}  '.format(o[0],o[1]), file=log)
        #r = eval_corr(archy.model, mode, normalize=True)
        #o = plot_correlation(r, output_name='norma_{}_{}_{}'.format(folds,mode,configfile))
        #print('corr_norma_{}  {}  {}  {}  {}  {}'.format(folds, np.mean(r),np.median(r),np.std(r), np.min(r), np.max(r),), file=log)
        #print('nperf_{}  {}  '.format(o[0],o[1]), file=log)
        folds +=1
    log.close()



    rfdata = pickle.load(open('../Data/RF_performance_GSStromo.pkl','rb'))
    gsd_results.append(rfdata)
    plot_correlation(gsd_results, output_name='GSD_{}_{}_{}_{}_{}'.format(folds,mode,configfile, en, rds))

    
    rfdata = pickle.load(open('../Data/RF_performance_GSStromo_MSE.pkl','rb'))
    gsd_results_mse.append(rfdata)
    plot_correlation(gsd_results_mse, output_name='GSDMSE_{}_{}_{}_{}_{}'.format(folds,mode,configfile, en, rds))
    

    # HamedRF_benchMArcus_MSEgeneral.pkl
    i = pickle.load(open('HamedRF_benchMArcus_MSEgeneral.pkl','rb'))
    vald_mse.append(i)
    plot_correlation(vald_mse, output_name='EVAL_{}_{}_{}_{}_{}'.format(folds,mode,configfile, en, rds),  swarm=False)

    # HamedRF_benchTim_MSEgeneral.pkl
    i = pickle.load(open('HamedRF_benchTim_MSEgeneral.pkl','rb'))
    hamed_mse.append(i)
    plot_correlation(hamed_mse, output_name='TH_{}_{}_{}_{}_{}'.format(folds,mode,configfile, en, rds), swarm=False)
    
    # HamedRF_benchNoyesF2_MSEgeneral.pkl
    i = pickle.load(open('HamedRF_benchNoyesF2_MSEgeneral.pkl','rb'))
    f2_mse.append(i)
    plot_correlation(f2_mse, output_name='F2_{}_{}_{}_{}_{}'.format(folds,mode,configfile, en, rds), swarm=False)

    # HamedRF_benchNoyesF3_MSEgeneral.pkl    
    i = pickle.load(open('HamedRF_benchNoyesF3_MSEgeneral.pkl','rb'))
    f3_mse.append(i)
    plot_correlation(f3_mse, output_name='F3_{}_{}_{}_{}_{}'.format(folds,mode,configfile, en, rds), swarm=False)

#plt.boxplot(cnn_toplot_mse_triplet)



