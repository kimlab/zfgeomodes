import json
import random
import hashlib
import sys
import os.path


run_script = """#!/bin/bash
#SBATCH -n 2 # number of cores
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --mem 8000 # memory pool for all cores
#SBATCH -t 0-8:05 # time (D-HH:MM)
#SBATCH --job-name=PFM_{}
#SBATCH -o slurm.%N.%j.out # STDOUT
#SBATCH -e slurm.%N.%j.err # STDERR
#module load python/3.5
#module load cuda cudnn
#source $HOME/tensorflow/bin/activate

source activate keras



python main.py {} {}

"""


for i in range(int(sys.argv[1])):
    filter1 = random.choice([16,32,64,92])
    ksize1 = random.choice([1,2,3,4])

    stride1 = random.choice([1,2,3])
    output1 = int((8-ksize1) /  stride1  ) + 1



    pool = list(range(1,output1))
    if len(pool) == 0:
            pool = [1]

    ksize2 = random.choice(pool)
    filter2 = random.choice([32,64,92,128])
    stride2 =  random.choice([1,2,3])

    output2 = int((output1-ksize2) /  stride2  ) + 1



    pool = list(range(1,output2))
    if len(pool) == 0:
            pool = [1]


    ksize3 = random.choice(pool)
    filter3 = random.choice([64,128])






    dense1 = random.choice([128,256])
    dense2 = int(dense1/2)
    dense3 = int(dense2/2)

    # optimizer
    lr = random.choice([.01,.001,.0001])
    decay =  lr/20
    lr = random.choice([.01,.001,.0001])
    opt = random.choice(['RMSprop','ADAM'])

    losser = random.choice(['MSE',])

    final = random.choice(['softmax', 'sigmoid', 'relu', 'linear'])
    drop = random.choice([0,.1,.2,.25,.5])

    regular = random.choice([None])
    if regular != None:
            val_reg = random.choice([.00001, .000001, .0000001])
    else:
            val_reg = 0

    # strides=1
    atemplate3 = [('Conv1D', {'filters':filter1 ,
                    'kernel_size':ksize1,
                    'activation':'relu','strides':stride1 }),

            ('Conv1D', {'filters':filter2 ,
                    'kernel_size':ksize2,
                    'activation':'relu','strides':stride2 }),
            #('Dropout',{'rate':.15}),
            ('Conv1D', {'filters':filter3 ,
                    'kernel_size':ksize3,
                    'activation':'relu',}),
            #('Dropout',{'rate':.15}),
            ('Flatten',),

            ('Dense', {'units':dense1, 'activation':'relu'}),
            ('Dropout',{'rate':drop}),
            ('Dense', {'units':dense2, 'activation' :'relu'}),
            ('Dropout',{'rate':drop}),

    ]

    atemplate4 = [('Conv1D', {'filters':filter1 ,
                    'kernel_size':ksize1,
                    'activation':'relu','strides':stride1 }),

            ('Conv1D', {'filters':filter2 ,
                    'kernel_size':ksize2,
                    'activation':'relu','strides':stride2 }),
            #('Dropout',{'rate':.25}),

            ('Flatten',),

            ('Dense', {'units':dense1, 'activation':'relu'}),
            ('Dropout',{'rate':drop}),
            ('Dense', {'units':dense2, 'activation':'relu'}),
            ('Dropout',{'rate':drop}),

    ]

    atemplate2 = [('Conv1D', {'filters':filter1 ,
                    'kernel_size':ksize1,
                    'activation':'relu','strides':stride1 }),

            ('Conv1D', {'filters':filter2 ,
                    'kernel_size':ksize2,
                    'activation':'relu','strides':stride2 }),
            (('Maxpool'),),
            #('Dropout',{'rate':.2}),

            ('Flatten',),

            ('Dense', {'units':dense1, 'activation':'relu'}),
            ('Dropout',{'rate':drop}),
            ('Dense', {'units':dense2, 'activation':'relu'}),
            ('Dropout',{'rate':drop}),

    ]


    atemplate1 = [('Conv1D', {'filters':filter1 ,
                    'kernel_size':ksize1,
                    'activation':'relu','strides':stride1 }),

            ('Conv1D', {'filters':filter2 ,
                    'kernel_size':ksize2,
                    'activation':'relu','strides':stride2 }),
            #('Dropout',{'rate':.15}),
            ('Conv1D', {'filters':filter3 ,
                    'kernel_size':ksize3,
                    'activation':'relu',}),
            ('Maxpool',),
            ('Flatten',),

            ('Dense', {'units':dense1, 'activation':'relu'}),
            ('Dropout',{'rate':drop}),
            ('Dense', {'units':dense2, 'activation' :'relu'}),
            ('Dropout',{'rate':drop}),

    ]



    template = [atemplate2,atemplate3,atemplate4,atemplate1]
    basic = random.randint(0,3)


    signature = '{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}'.format(regular,val_reg,stride1,stride2,drop,final,filter1,filter2,ksize1,ksize2,dense1,dense2,dense3,lr,decay,opt,basic)

    name = hashlib.md5(signature.encode('utf-8')).hexdigest()

    if os.path.isfile(name+'.json'):
        continue
    else:
        #arch
        configuration  = {'archy':template[basic],
                        'optim': [opt,{'decay':decay}],
                        'lr':lr,
                        'loss_on':losser,
                        'finalAct':final}
        print(configuration)
        json.dump(configuration,fp=open('{}.json'.format(name),'w'))

        for a in ['UNIQUE']:
                        s = run_script.format(  name,a, name )
                        o = open('job_setup.sh', 'w')
                        print(s, file= o)
                        o.close()
                        os.system('sbatch  -A def-pmkim  job_setup.sh ')
                        #os.system('python main.py UNIQUE {}'.format( name))


