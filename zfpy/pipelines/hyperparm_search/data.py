import sys
import json
import os
import pickle
import random
import argparse
import math
import re
from collections import defaultdict
import numpy as np
import pandas as pd
import json
import glob
import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import pearsonr
from scipy.stats import entropy
from sklearn.metrics import mean_squared_error



from sklearn.utils import shuffle
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold

from multiprocessing import Pool
from functools import partial

import math


##

all_amino = ['_','C', 'V', 'T', 'F', 'Y', 'A', 'P', 'W', 'I', 'M', 'L', 'S', 'G', 'H', 'D', 'E', 'N', 'Q', 'R', 'K']
n_amino = len(all_amino)
aa_to_int = dict((c, i) for i, c in enumerate(all_amino))

int_to_aa = dict((c, i) for i, c in aa_to_int.items())



aa_one_hot = dict()
for a,i in aa_to_int.items():
    v = np.zeros(len(aa_to_int))
    v[i] = 1
    aa_one_hot[a] = v



triplets = list()
bases = ['A','C' ,'G', 'T']

for b1 in bases:
    for b2 in bases:
        for b3 in bases:
            triplets.append(b1+b2+b3)

n_bases = len(bases)

base_to_int = dict((c, i) for i, c in enumerate(bases))
int_to_base = dict((c, i) for i, c in base_to_int.items())



base_one_hot = dict()
for a,i in base_to_int.items():
    v = np.zeros(len(base_to_int))
    v[i] = 1
    base_one_hot[a] = v


#def
def pwm2freq(vectors):

    convert = lambda x: math.pow(10,x) /4.0
    matrix = list()
    for i in range(0,len(vectors),4):
        matrix.append(vectors[i:i+4])
    m =  pd.DataFrame(matrix, index=list(range(3)),columns=( 'A','C' ,'G', 'T')  )
    #print(m)
    m = m.applymap(convert)
    return np.reshape(m.values,(12))

    return m


def _itersplit_motif(zfinger):
    region = list()
    # Reg 0
    zfinger = 'X'+ zfinger
    re_motifs = [ '.{1,5}C.{11,15}H.{2,6}[HC]', '.{1,5}C.{11,15}H',]

    seq = re.findall(re_motifs[0],zfinger )
    if seq:
        seq = re.findall(re_motifs[1],seq[0])


    return seq



def extract_spec_res_tim(zfinger):
    parts = list()
    zregion = _itersplit_motif(zfinger)
    if len(zregion) !=0:
        for i in [-8,-7,-6,-5,-3,-2]:
            parts.append(zregion[0][i:i+1])
        return ''.join(parts)
    return None


def extract_spec_res_GSD(zfinger):
    parts = list()
    #zregion = itersplit_motif(zfinger)
    #print(zregion)
    if len(zfinger) !=0:
        for i in [-8,-7,-6,-5,-3,-2]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None



def extract_spec_res_marcus(zfinger):
    parts = list()
    #zregion = itersplit_motif(zfinger)
    if len(zfinger) !=0:
        for i in [8,9,10,11,13,14]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None

def extract_spec_res_tim_mode(zfinger):
    parts = ['R']
    zregion = _itersplit_motif(zfinger)
    if len(zregion) !=0:
        for i in [-9,-8,-7,-6,-5,-3,-2]:
            parts.append(zregion[0][i:i+1])
        return ''.join(parts)
    return None




def norma(flat_res):
    # linear activations can return negative numbers
    flat_res = flat_res.clip(min=0.0)
    r = list()
    for i in range(0,12,4):
        r.append(flat_res[i:i+4] / flat_res[i:i+4].sum())
    return np.asarray(r).flatten()


def onehot_seq(seq, kind = 'aa'):
     if kind == 'aa':
         one_hot = aa_one_hot
     else:
         one_hot = base_one_hot

     _one_hot_seq =  list()
     for s in seq:
         _one_hot_seq.append(one_hot[s])

     return np.asarray(_one_hot_seq)




# def addboundaryresidues(data,y,m):
#     expanded = list()
#     y_aug = list()
#     bres = {'RS':['RS', 'RT', 'RN', 'RC','RK','RG', 'RR', 'RP'],
#             'RA':['RA', 'RT', 'RN', 'RC','RK','RG', 'RR', 'RP'],
#             'MODE2':['RR', 'WR', 'MR', 'LR', 'QR', 'NR', 'RK', 'YR', 'KR', 'TR'],
            
#             'MODE3':['VS', 'TH', 'TS', 'HN', 'HS', 'IA', 'TA', 'NK', 'TT', 'IS', 'YS'],
#             'MODE4':['KS', 'RT', 'KT', 'LT', 'MT','RS', 'NT', 'IT', 'QT', 'KS', 'HT'],
#             'MODE5':['IN', 'LN', 'VN', 'MN', 'FN', 'YN','TN','IT','PN', 'NN'],
#             'MODE6':['KR', 'MR', 'RR', 'TR', 'LR', 'QR', 'SR','KT', 'NR','QT']}


#     bres = {'RS':['RS', 'RT', 'RN', 'RC','RK','RG', 'NA','RR', 'RP'],
#             'RA':['RA', 'RT', 'RN', 'RC','RK','RG', 'NA','RR', 'RP'],
#             'MODE2':['RR', 'LR', 'QR', 'NR', 'RK', 'KR', 'TR'],
#             'MODE3':['VS', 'TS', 'HN', 'HS', 'TT', 'IS'],
#             'MODE4':['KS', 'RT', 'KT', 'LT', 'MT', 'RS', 'NT', 'IT', 'QT', 'KS', 'HT'],
#             'MODE5':['IN', 'LN', 'VN', 'TN', 'IT', 'NN'],
#             'MODE6':['KR', 'RR', 'TR', 'LR', 'QR', 'SR', 'KT', 'NR', 'QT']}
    
#     for idx, X in enumerate(data):
        
#         for bpairs in bres[m]:
#             temp = np.reshape(X,(6,21))
#             extra = onehot_seq(bpairs)
            
#             temp = np.concatenate([extra, temp])
#             temp = np.reshape(temp,(8,21,1))
#             expanded.append(temp)
#             y_aug.append(y[idx])

#     return np.asarray(expanded), np.asarray(y_aug)

def addboundaryresidues(data,m):
    expanded = list()
    bres = {'RS':'RS','RA':'RA','MODE2':'RR','MODE3':'VS','MODE4':'KS', 'MODE5':'IN','MODE6':'KR'}
    extra = onehot_seq(bres[m])
    for X in data:
        temp = np.reshape(X,(6,21))
        temp = np.concatenate([extra, temp])
        temp = np.reshape(temp,(8,21,1))
        expanded.append(temp)

    return np.asarray(expanded)

def tag_mode(MBP):

    modes = {'RS':['RS'],'RA':['RA'],
             'MODE1':['LI', 'RG', 'QK','RN','VT','II','VY'],
             'MODE2':['RT','RM','RR','LH',],
             'MODE3':['NA','VK','VS','NP',],
             'MODE4':['LS', 'YS','KS','FS','TA','WR',],
             'MODE5':['KI','HV','QV','YF','RQ', 'YC', 'IN', 'NS'],
             'MODE6':['HT','KR','RV','LR','KV']}

    for label, residues in modes.items():
        if MBP in residues:
            return label

    return 'UNK'


def load_timdata():

    tim = pd.read_csv('../Data/B1H.motifs_spec.csv')
    tim['res_m'] = tim['ZF_sequence'].apply(extract_spec_res_tim_mode)
    tim =  tim[~tim['res_m'].isnull()]

    dataX_Tim = list()
    dataY_Tim = list()
    for idx, row in tim.iterrows():

        seq_out = row['res_m'].replace('X','A')
        #print(seq_out)
        query = onehot_seq(seq_out, kind = 'aa')
        #while len(query) < 29:
        #    query.append(0)

        query = np.array(query)
        #print(query)
        query = np.reshape(query,(8,21))
        #target = [to_one_hot[char] for char in seq_out]
        features = [ 'A1', 'C1', 'G1', 'T1', 'A2', 'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3'] #'A4', 'C4', 'G4', 'T4',]

        #target = row[features].values

        target = pwm2freq(row[features].values)
        #print(target)
        #target = __convertscore2freq(target)
        #dataX.append(np.reshape(result, (1, result.shape[0], result.shape[1])))
        #dataY.append(np.array(to_one_hot[seq_out]))
        dataX_Tim.append(query)
        dataY_Tim.append(target)

    X =  np.asarray(dataX_Tim)

    y = np.asarray(dataY_Tim)

    X, y = shuffle(X,y)

    return X, y


def load_F2data():

    foldx = pickle.load(open('../Data/F2_Xtotal_nr_clean.pkl', 'rb'))
    y = pickle.load(open('../Data/F2_ytotal_nr_clean.pkl', 'rb'))

    expanded = list()
    for X in foldx:
            extra = onehot_seq('RA')
            temp = np.reshape(X,(6,21))
            temp = np.concatenate([extra, temp])
            temp = np.reshape(temp,(8,21))
            expanded.append(temp)
    x = np.asarray(expanded)

    return x, y


def load_F3data():

    foldx = pickle.load(open('../Data/F3_Xtotal_nr_clean.pkl', 'rb'))
    y = pickle.load(open('../Data/F3_ytotal_nr_clean.pkl', 'rb'))

    expanded = list()
    for X in foldx:
            extra = onehot_seq('RS')
            temp = np.reshape(X,(6,21))
            temp = np.concatenate([extra, temp])
            temp = np.reshape(temp,(8,21))
            expanded.append(temp)
    x = np.asarray(expanded)

    return x, y



def mse_pool(f3oldX,f3oldy, model):
    f3_mse = list()
    for idx, x in enumerate(f3oldX):
        query = np.reshape(x, (1,8,21))
        e = model.predict(query)
        r = mean_squared_error(e.flatten(),f3oldy[idx].astype(float))
        f3_mse.append(r)

    return f3_mse

def corr_pool(f3oldX,f3oldy, model):

    lstm_f3_corr = list()
    for idx, x in enumerate(f3oldX):
        query = np.reshape(x, (1,8,21))
        e = model.predict(query)
        r = compare_motifs(e.flatten(),f3oldy[idx].astype(float))
        lstm_f3_corr.append(r)

    return lstm_f3_corr



class DataPrep(object):
    """docstring for Datapreprocessing."""
    def __init__(self, folder_data = '../Data/', read_cutoff = 5, entropy_cutoff = 0.05, reads_fn = 'cond' ):
        #super(Datapreprocessing, self).__init__()
        self.folder_data = folder_data
        self.rcut = read_cutoff
        self.ecut = entropy_cutoff
        self.rfn = reads_fn

        # path, o mask, to label with type of data I will work
        self.X = list()

    def load_unify(self):

        #mode = 'MODE5'


        X, y =  self._merge_modes()

        test_XM, test_yM = self._merge_modes(dtype='test')

        self.y = y
        self.test_yM = test_yM

        #folder_data = '/home/kimlab2/ccorbi/MARCUS_ZF/preprocess/'

        #X = pickle.load(open('{}/{}_Xtrain.pkl'.format(folder_data,mode),'rb'))
        self.X = np.reshape(X, (X.shape[0], 8, 21))

        #self.y = pickle.load(open('{}/{}_ytrain.pkl'.format(folder_data,mode),'rb'))

        #test_XM = pickle.load(open('{}{}_Xtest.pkl'.format(folder_data,mode),'rb'))
        self.test_XM = np.reshape(test_XM, (test_XM.shape[0], 8, 21))

        #self.test_yM = pickle.load(open('{}{}_ytest.pkl'.format(folder_data,mode),'rb'))


    def load_mode(self, mode):

        #mode = 'MODE5'

        #folder_data = '/home/kimlab2/ccorbi/MARCUS_ZF/preprocess/'

        X = pickle.load(open('{}/{}_Xtrain_{}_{}_{}.pkl'.format(self.folder_data,mode,self.rfn,self.rcut, self.ecut),'rb'))
        self.X = np.reshape(X, (X.shape[0], 6, 21))

        self.y = pickle.load(open('{}/{}_ytrain_{}_{}_{}.pkl'.format(self.folder_data,mode,self.rfn,self.rcut, self.ecut),'rb'))

        test_XM = pickle.load(open('{}/{}_Xtest_{}_{}_{}.pkl'.format(self.folder_data,mode,self.rfn,self.rcut, self.ecut),'rb'))
        self.test_XM = np.reshape(test_XM, (test_XM.shape[0], 6, 21))

        self.test_yM = pickle.load(open('{}/{}_ytest_{}_{}_{}.pkl'.format(self.folder_data,mode,self.rfn,self.rcut, self.ecut),'rb'))


    def kf(self,splits=10, seed=42):
        #concat data set and load
        if len(self.X)==0:
            self.load_unify()

        self.totx = np.concatenate([self.X, self.test_XM])
        self.toty = np.concatenate([self.y, self.test_yM])

        kf = KFold(n_splits=splits, random_state=seed  )
        kf.get_n_splits(self.totx)

        kf.get_n_splits(self.totx)
        for train_index, test_index in kf.split(self.totx):
            yield self.totx[train_index], self.toty[train_index], self.totx[test_index], self.toty[test_index]




    def _merge_modes_augmentation(self, dtype='train'):



        superX = list()
        supery = list()

        for a in [ 'MODE6',  'RS', 'MODE4', 'MODE3','MODE5', 'MODE2']:
            X = pickle.load(open('{}/{}_X{}_{}_{}_{}.pkl'.format(self.folder_data,a, dtype,self.rfn,self.rcut, self.ecut),'rb'))
            y = pickle.load(open('{}/{}_y{}_{}_{}_{}.pkl'.format(self.folder_data,a,dtype ,self.rfn,self.rcut, self.ecut),'rb'))

            X, y = addboundaryresidues(X,y, a)

            for i in X:
                superX.append(i)

            for i in y:
                supery.append(i)


        superX = np.asarray(superX)
        supery = np.asarray(supery)

        superX, supery = shuffle(superX,supery)

        return superX, supery


    def _merge_modes(self, dtype='train'):



        superX = list()
        supery = list()

        for a in [ 'MODE6',  'RS', 'MODE4', 'MODE3','MODE5', 'MODE2']:
            X = pickle.load(open('{}/{}_X{}_{}_{}_{}.pkl'.format(self.folder_data,a, dtype,self.rfn,self.rcut, self.ecut),'rb'))
            y = pickle.load(open('{}/{}_y{}_{}_{}_{}.pkl'.format(self.folder_data,a,dtype ,self.rfn,self.rcut, self.ecut),'rb'))

            X = addboundaryresidues(X, a)

            for i in X:
                superX.append(i)

            for i in y:
                supery.append(i)


        superX = np.asarray(superX)
        supery = np.asarray(supery)

        superX, supery = shuffle(superX,supery)

        return superX, supery


    def transform2PWM(self):


        a = self.y + .01
        self.y = np.log(a.astype(float))

        a = self.test_yM + .01
        self.test_yM = np.log(a.astype(float))
        a =0

    def _simple_PWM(self,y):
        t = list()
        for x in y:
            if x == 1:
                t.append(0.0)
            else:
                x = x+.01
                t.append(-np.log(x))
        return np.asarray(t)

    def trans(self):
               # a = self.y + .01
        self.y = np.apply_along_axis(self._simple_PWM,0, self.y)

        #a = self.test_yM + .01
        self.test_yM = np.apply_along_axis(self._simple_PWM,0, self.test_yM)
        #a =0



# EVALUATE MODEL
# LOAD SEQUENCES

poolSeqs = pickle.load(open('rand_seq_50gc_10K.pkl','rb'))


def to_PSAM(flat_res):
    y = list()
    l = int(flat_res.shape[0]/4)
    for i in range(0,flat_res.shape[0],4):
        y.append(flat_res[i:i+4] / flat_res[i:i+4].max())
    m = np.asarray(y)
    m = np.reshape(m, (l,4))
    return  pd.DataFrame(m, index=list(range(l)),columns=('A','C','G','T')  )


def binding_prob(seq, psam):

    score = list()
    for pos, symbol in enumerate(seq):
        score.append(psam.at[pos,symbol])

    val_score = np.prod(score, dtype=float)
    try:
        assert val_score != 0
        val_score = 1 +(1/val_score)
        assert val_score != 0
        val_score =  1/val_score
        assert val_score != 0

        return val_score
    except AssertionError as error:
        print(error)
        print(seq)
        print(score, type(score))
        print(val_score, type(val_score))
        print(psam)


def norm_prediction(flat_res):
    r = list()
    flat_res = flat_res.clip(min=0.0, max=1.0)
    for i in range(0,flat_res.shape[0],4):
        r.append(flat_res[i:i+4] / flat_res[i:i+4].sum())
    return np.asarray(r).flatten()


def compare_motifs(pred, targ):
    v = {0:[],1:[]}

    targ = targ + 0.00001
    pred = pred + 0.00001

    l = int(len(targ) /4)
    l2 = int(len(pred) /4)
    total_len =  l2 + l

    for i,matrix in enumerate([pred,targ]):
        psam = to_PSAM(matrix)
        for seq in poolSeqs:
            seq_score = list()
            for triplet in range(0,total_len+1, psam.shape[0]):
                subseq = seq[triplet:triplet+psam.shape[0]]
                if len(subseq) == psam.shape[0]:
                    seq_score.append(binding_prob(subseq, psam))
            #print(seq_score)
            v[i].append(sum(seq_score))

        v[i] = np.log(v[i])

    return pearsonr(v[0],v[1])[0]


#################################
#future


def compare_motifs_MP(data_set):

    pred, targ = data_set

    v = {0:[],1:[]}

    targ = targ + 0.00001
    pred = pred + 0.00001

    l = int(len(targ) /4)
    l2 = int(len(pred) /4)
    total_len =  l2 + l

    for i,matrix in enumerate([pred,targ]):
        psam = to_PSAM(matrix)
        for seq in poolSeqs:
            seq_score = list()
            for triplet in range(0,total_len+1, psam.shape[0]):
                subseq = seq[triplet:triplet+psam.shape[0]]
                if len(subseq) == psam.shape[0]:
                    seq_score.append(binding_prob(subseq, psam))
            #print(seq_score)
            v[i].append(sum(seq_score))

        v[i] = np.log(v[i])

    return pearsonr(v[0],v[1])[0]

# MAIN


def squeez(fingers):
    features = [ 'A1', 'C1', 'G1', 'T1', 'A2',
       'C2', 'G2', 'T2', 'A3', 'C3', 'G3', 'T3' ]

    matrix = fingers[features].values.flatten()

    return matrix

def evaluate_GSD(model, dummy=None):

    GSgs2014dm = pd.read_csv('../Data/GStormo_DM.csv' , keep_default_na=False)

    results = dict()
    for idx, tf in GSgs2014dm.groupby('TFid', as_index=False):
        targ = squeez(tf)
        n = tf['TFid'].unique()[0].strip()
        resort = tf.sort_values('finger', ascending=False)
        nn_pred = list()
        for idj, finger in tf.iterrows():
            seq = finger['MODE']+ finger['SRES']
            query = onehot_seq(seq, kind = 'aa')
            query = np.reshape(query,(1,8,21))
            res = model.predict(query)
            flat_res = np.reshape(res, (12))
                #print(flat_res)
            flat_res =norm_prediction(flat_res)
            nn_pred.append(flat_res)
        nn_pred = np.asarray(nn_pred).flatten().astype(float)
        values = compare_motifs(targ.astype(float), nn_pred)
        results[n] = values

    return results


def evaluate_GSD_MSE(model, dummy=None):

    GSgs2014dm = pd.read_csv('../Data/GStormo_DM.csv' , keep_default_na=False)

    results = dict()
    for idx, tf in GSgs2014dm.groupby('TFid', as_index=False):
        targ = squeez(tf)
        n = tf['TFid'].unique()[0].strip()
        resort = tf.sort_values('finger', ascending=False)
        nn_pred = list()
        for idj, finger in tf.iterrows():
            seq = finger['MODE']+ finger['SRES']
            query = onehot_seq(seq, kind = 'aa')
            query = np.reshape(query,(1,8,21))
            res = model.predict(query)
            flat_res = np.reshape(res, (12))
                #print(flat_res)
            flat_res =norm_prediction(flat_res)
            nn_pred.append(flat_res)
        nn_pred = np.asarray(nn_pred).flatten().astype(float)
        values = mean_squared_error(targ.astype(float), nn_pred)
        results[n] = values

    return results


def evaluate_complex_GSD_MP(model, method='compare_motif_within'):

    p = Pool(4)

    gsd = pickle.load(open('pool_GSDc2019_v3.pkl','rb'))

    pairs_2_eva = list()

    for tf in gsd:
        preds = generate_preds(tf, model)
        pairs_2_eva.append((tf, preds))


    #eval_function = partial(single_eval,tf_func=method, model=model)
    #eval_function = partial(single_eval,tf_func=method, model=[0.0,]*12)
    eval_function = partial(single_eval,tf_func=method)

    resutls = p.map(eval_function, pairs_2_eva)


    return resutls




def generate_preds(tf, model):

    preds_pool = list()
    for idx, array in tf.fingers.groupby('ARRAY', as_index=False):


        if array.shape[0] > 1:
            concat_pred = list()
            array.sort_values('Motif_FromPos', ascending=False, inplace=True)
            for idx, finger in array.iterrows():

            # 53


                seq_out = finger['SPECRES'].replace('X','_')
                if '_' in seq_out:
                    continue
                #reprocess = extract_motifs(seq_out)

                query = onehot_seq(seq_out, kind = 'aa')
                try:
                    extra = onehot_seq(finger['MODE'].replace('X','_'))
                except TypeError:
                    print('error {}'.format(finger['MODE']))
                    extra = onehot_seq('__')

                #mes.append(row['TAG_MODE'])

                temp = np.concatenate([extra, query])
                query = np.reshape(temp,(1,8,21))



                #query = np.array(query)
                #query = np.reshape(query,(1, query.shape[0],query.shape[1]))

                res = model.predict([query,query,query,query])
                #res = model
                flat_res = np.reshape(res, (12))
                #print(flat_res)
                flat_res =norm_prediction(flat_res)
                print(flat_res)
                concat_pred.append(flat_res)

            if len(concat_pred)!=0:
                concat_pred = np.asarray(concat_pred).flatten()
                preds_pool.append(concat_pred)
                #preds_pool.append(getattr(tf, tf_func)(concat_pred))

    return preds_pool



from operator import itemgetter
def max_val(l, i):
    return max(enumerate(map(itemgetter(i), l)),key=itemgetter(1))

class TF_prediction(object):
    """docstring for ClassName."""
    def __init__(self, tf_id):

        self.TF_id = tf_id

        self.arrays_within = list()
        self.arrays_flanking = list()



    def add_within_comparation(self, comp):

        if self.valid(comp):

            # overlap
            n = comp[2][1] - comp[2][0]
            r = comp[0]
            t = self.tvalue(r,n)

            self.arrays_within.append((t,r,n))


    def add_flanking_comparation(self, comp):

        if self.valid(comp):

            # overlap
            n1 = comp[2][1] - comp[2][0]
            n2 = comp[2][4] - comp[2][3]
            n = min([n1, n2])
            r = comp[0]
            t = self.tvalue(r,n)

            self.arrays_flanking.append((t,r,n))



    def get_best(self, method = 'within'):

        if method == 'compare_motif_within':
            arrays = self.arrays_within
        else:
            arrays = self.arrays_flanking

        if len(arrays) == 0:
            return [0,0,0,]
        if len(arrays) == 1:

            return arrays[0]
        idx, val = max_val(arrays, 0)

        return arrays[idx]


    def tvalue(self, r,n):
        return r/ (np.sqrt( (1-r**2)/(n-2) ))

    def valid(self,comp):
        if comp == None or comp[0]== -10:
            return False
        else:
            return True


def single_eval( data_pairs, tf_func):

    tf, preds = data_pairs
    results_pool = TF_prediction(tf.TF_id)
    for p in preds:

        #results_pool.append(
        pred = getattr(tf, tf_func)(p)
        if tf_func=='compare_motif_within':
            results_pool.add_within_comparation(pred)
        else:
            results_pool.add_flanking_comparation(pred)


    # select best array
    return results_pool.get_best(tf_func)

        #print('Something went wrong {}'.format(tf.TF_id))
        #print(results_pool)
        #return [0,0,0,]





#################33
#################
def pickmaximum(pool_preds):
    maxim = -10
    res = None
    for i in pool_preds:
        if i[0] != None:
            if i[0]> maxim:
                maxim = i[0]
                res = i
    return maxim, res


class TFmotif(object):
    """docstring for TF."""
    def __init__(self, motif_info, motif_matrix, motif_features, fullseq = None , verbose = 0 ):

        self.verbose =  verbose

        self.TF_id = motif_info['TF_ID'].values[0]
        self.TF_name = motif_info['TF_Name'].values[0]
        self.Sp = motif_info['TF_Species'].values[0]
        self.motif_id = motif_info['Motif_ID'].values[0]


        self.source =  motif_info['MSource_Type'].values[0]

        self.year =  motif_info['MSource_Year'].values[0]

        self.DBID =  motif_info['DBID.1'].values[0]



        self.pfm = self._preprocess_matrix(motif_matrix)
        self.motif_nbases = motif_matrix.shape[0]


        self.arrays, self.fingers = self._preprocess_features(motif_features)
        self.nfingers = self.fingers.shape[0]


        self.full_seq = fullseq




    def _preprocess_matrix(self, matrix):
        matrix.rename(columns={'A':'5A',
                  'C':'5C',
                  'G':'5G',
                  'T':'5T',}, inplace=True)
        #A4 = T1
        matrix['3T'] = matrix['5A'].iloc[::-1]
        #C4 = G1
        matrix['3G'] = matrix['5C'].iloc[::-1]
        #G4 = C1
        matrix['3C'] = matrix['5G'].iloc[::-1]
        #T4 = A1
        matrix['3A'] = matrix['5T'].iloc[::-1]

        features = ['Pos','5A','5C','5G','5T', '3A','3C','3G','3T']

        return  matrix[features]

    def _preprocess_features(self, motif_features):
            df = motif_features.sort_values('Motif_FromPos', ascending=True)
            df.reset_index(drop=True, inplace=True)
            m0 = 'R'

            df['MODE'] = ''
            df['SPECRES'] = ''
            df['ARRAY'] = None
            df['ENGADGE'] = None
            prev = 0
            array = 0

            for idx, row in df.iterrows():

                    s = row['MotifFeature_Sequence'].strip()
                    #print(s)
                    try:
                        core = re.findall(r'C.{2,4}C.{12,15}H..', s)[0]

                        specres = list()
                        for i in [-10,-9,-8,-7,-5,-4]:
                            specres.append(core[i])


                        m1 = core[-11]


                    except IndexError:
                    #if len(core) == 0:
                        # no canonical
                        if self.verbose > 0:
                            print('ERROR EXTRACTING {} {}'.format(self.motif_id, s))

                        df.at[idx, 'ENGADGE'] = 'SKIP'
                        m1= 'X'
                        specres = ['X','X','X','X']
                        try:
                            ct = re.findall(r'H.{3,5}H$', s)[0]
                            core = ct[2]
                            core  = [core]
                        except IndexError:
                            print('Can not extract +9')
                            core = ['R']

                    #print(core)
                    #print(core[-1:])
                    #m0 = core[-1:]
                    #m1 = core[6]
                    #print(m0,m1)

                    #for i in [7,8,9,10,12,13]:
                    #    specres.append(core[i:i+1])
                    #print(m0,m1,''.join(specres))
                    if row['Motif_FromPos']-prev >7:

                        array = array + 1
                    prev = row['Motif_ToPos']

                    df.at[idx, 'ARRAY'] = array
                    df.at[idx, 'MODE'] = m0+m1
                    df.at[idx, 'SPECRES'] = ''.join(specres)

                    m0 = core[-1]

            return array, df[df['ENGADGE']!='SKIP']


    def _compare_motif(self,pred, target ):



        v = {0:[],1:[]}

        target = target + 0.00001
        pred = pred + 0.00001

        l = int(len(target) /4)
        l2 = int(len(pred) /4)
        total_len =  l2 + l
        #target = self.pfm[[ '5A','5C' , '5G', '5T' ]].values.flatten()

        #add pseudocounts



        for i,matrix in enumerate([pred,target]):
            psam = to_PSAM(matrix)
            for seq in poolSeqs:
                seq_score = list()
                for triplet in range(0,total_len+1, psam.shape[0]):
                    subseq = seq[triplet:triplet+psam.shape[0]]
                    if len(subseq) == psam.shape[0]:
                        seq_score.append(binding_prob(subseq, psam))
                #print(seq_score)
                v[i].append(sum(seq_score))

            v[i] = np.log(v[i])

        return pearsonr(v[0],v[1])[0]

#class

    def compare_motif_global(self, pred):

        if self.motif_nbases ==0:
            return None


        target = self.pfm[[ '5A','5C' , '5G', '5T' ]].values.flatten()

        #add pseudocounts

        return self._compare_motif(pred, target)


    def compare_motif_within(self, pred):

        if self.motif_nbases ==0:
            return None


        best_corr = -10
        best_mse = 10
        coordinates =  list()

        target = self.pfm[[ '5A','5C' , '5G', '5T' ]].values.flatten()

        #add pseudocounts

        target = target + 0.00001
        pred = pred + 0.00001

        if pred.shape[0] < target.shape[0]:
            large = target
            short = norm_prediction(pred)
            label = 'target'


        else:

            large = norm_prediction(pred)
            short = target
            label = 'pred'


        end = large.shape[0] - short.shape[0] + 4
        for idx in range(0, end, 4):
            sub_large = large[idx:idx+short.shape[0]]
            #target = target[features].values.flatten()
            corr = self._compare_motif(short, sub_large)


            if corr > best_corr:
                best_corr = corr
                best_mse = mean_squared_error(short, sub_large)
                coordinates = [idx,idx+short.shape[0], label]


        return best_corr, best_mse, coordinates



    def compare_motif_flanks(self, pred, minimum_overlap = 4):

        if self.motif_nbases ==0:
            return None

        best_corr = -10
        best_mse = 10
        coordinates =  list()

        target = self.pfm[[ '5A','5C' , '5G', '5T' ]].values.flatten()

        #add pseudocounts

        target = target + 0.00001
        pred = pred + 0.00001



        if pred.shape[0] < target.shape[0]:
            large = target
            short = norm_prediction(pred)
            label = 'target'


        else:

            large = norm_prediction(pred)
            short = target
            label = 'pred'


        len_short = int(short.shape[0] /4)
        if len_short > minimum_overlap:
            for idx in range(1, len_short-1):

                sub_short = short[idx*4:]
                sub_long = large[0:sub_short.shape[0]]
                #target = test_tf.pfm[0:int(subpred.shape[0]/4)]
                #target = target[features].values.flatten()
                corr = self._compare_motif(sub_short, sub_long)


                if corr > best_corr:
                    best_corr = corr
                    best_mse = mean_squared_error(sub_short, sub_long)
                    coordinates = [0,idx*4, label,0,sub_short.shape[0],'5']

                sub_short = short[idx*4:]

                subindex = large.shape[0]-sub_short.shape[0]
                sub_long = large[subindex:]
                corr = self._compare_motif(sub_short, sub_long)

                if corr > best_corr:
                    best_corr = corr
                    best_mse = mean_squared_error(sub_short, sub_long)
                    coordinates = [idx*4,short.shape[0], label, subindex,large.shape[0],'3']


        return best_corr, best_mse, coordinates


    def __str__(self):
        return '{}:{} Mid:{}  Arrays:{}  Fingers:{}  Mlen:{}'.format(self.TF_id, self.TF_name, self.motif_id, self.arrays, self.nfingers, self.motif_nbases)
        #return

### PLOTTING

def plot_train(model_output, what='loss', output_name='test'):

    fig, ax = plt.subplots(figsize=(20,10))

        #ax.bar(df.columns,df.get_values()[0])
        #ax.set_title(sample_name)
    #plt.xticks(df.columns , df.columns, rotation='vertical')
    ax.plot(model_output.history['{}'.format(what)])
    ax.plot(model_output.history['val_{}'.format(what)])

    fig.savefig('train_{}.png'.format(output_name))
    plt.close(fig)

    return


def plot_correlation(model_output, what='loss', output_name='test', swarm=True):

    # load RF data
    #rfdata = pickle.load(open('../Data/RF_performance_GSStromo.pkl','rb'))
    #model_output.append(rfdata)
    fig, ax = plt.subplots(figsize=(10,10))

        #ax.bar(df.columns,df.get_values()[0])
        #ax.set_title(sample_name)
    #plt.xticks(df.columns , df.columns, rotation='vertical')
    #ax.plot(model_output.history['{}'.format(what)])
    #ax.plot(model_output.history['val_{}'.format(what)])
    sns.boxplot(data=model_output,  ax=ax, palette="vlag")
    if swarm:
        sns.swarmplot(data=model_output,  ax=ax, color=".3")
        ax.set_ylim(-1,1)
    ax.axhline(0)

    fig.savefig('corrValidation_{}.png'.format(output_name))
    plt.close(fig)

    return

def plot_gsd(results, output_name):

    hamed_GSD = pickle.load(open('rfh_bench_GSD.pkl','rb'))

    fig, ax = plt.subplots(1,2,figsize=(20,10))
    ax[0].boxplot([results[3],hamed_GSD[3],results[2],hamed_GSD[2]])

    ax[1].boxplot([results[1],hamed_GSD[1],results[0],hamed_GSD[0]])

    fig.savefig('GSD_{}.png'.format(output_name))
    plt.close(fig)

    return

def _counts(data, over=.5):
    c = 0
    for i in data:
        if float(i)>=over:
            c+=1
    return c


def removenans(result_cm):

    clean = list()
    for i in result_cm:
        for v in i:
            if v:
                #print('f')
                clean.append(v)
            else:
                pass
                #clean.append(v)
    return clean
