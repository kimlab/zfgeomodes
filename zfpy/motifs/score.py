import sys
import json
import os
import pickle
import random
import argparse
import math
import re
from collections import defaultdict, namedtuple
import numpy as np
import pandas as pd
import glob


from multiprocessing import Pool
from functools import partial

from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr

import logomaker


from  ..base import *
from ._base import *


def load_randomPoolSeqs():


    motifs_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    pool_file = './rand_seq_50gc_10K.pkl'
    abs_file_path = os.path.join(motifs_dir, pool_file)

    return pickle.load(open(abs_file_path,'rb'))

poolSeqs = load_randomPoolSeqs()



def binding_prob(seq, psam):

    score = list()
    for pos, symbol in enumerate(seq):
        score.append(psam.at[pos,symbol])

    val_score = np.prod(score, dtype=float)
    try:
        assert val_score != 0
        val_score = 1 +(1/val_score)
        assert val_score != 0
        val_score =  1/val_score
        assert val_score != 0

        return val_score
    except AssertionError as error:
        print(error)
        print(seq)
        print(score, type(score))
        print(val_score, type(val_score))
        print(psam)

def to_PSAM(flat_res):
    y = list()
    l = int(flat_res.shape[0]/4)
    for i in range(0,flat_res.shape[0],4):
        y.append(flat_res[i:i+4] / flat_res[i:i+4].max())
    m = np.asarray(y)
    m = np.reshape(m, (l,4))
    return  pd.DataFrame(m, index=list(range(l)),columns=('A','C','G','T')  )


def compare_motifs(pred, targ):
    v = {0:[],1:[]}

    targ = targ + 0.00001
    pred = pred + 0.00001

    l = int(len(targ) /4)
    l2 = int(len(pred) /4)
    total_len =  l2 + l

    for i,matrix in enumerate([pred,targ]):
        psam = to_PSAM(matrix)
        for seq in poolSeqs:
            seq_score = list()
            for triplet in range(0,total_len+1, psam.shape[0]):
                subseq = seq[triplet:triplet+psam.shape[0]]
                if len(subseq) == psam.shape[0]:
                    seq_score.append(binding_prob(subseq, psam))
            #print(seq_score)
            v[i].append(sum(seq_score))

        v[i] = np.log(v[i])

    return pearsonr(v[0],v[1])[0]



Aligment_results = namedtuple('Aligment_results', 'score up_matched bot_matched up_plot bot_plot')

def front_padding_vector(vector, n):
    padding_vector = np.zeros(n)
    return np.concatenate([padding_vector, vector])


def back_padding_vector(vector, n):
    padding_vector = np.zeros(n)
    return np.concatenate([vector, padding_vector ])

def slide_over_mse(a, b, minim_overlap=12):
    
    results = list()
    if a.shape[0]>b.shape[0]:
        bot = a
        up = b
    else:
        bot = b
        up = a
    
    
    #init coord

    modif_up_st = up.shape[0] -minim_overlap 
    modif_up_end = up.shape[0]


    modif_bot_st = 0
    modif_bot_end = minim_overlap
    
    
    up_len = up.shape[0]
    sub_up = up[modif_up_st:modif_up_end]
    
    
    bot_len = bot.shape[0]
    sub_bot = bot[modif_bot_st:modif_bot_end]
    
    sliding_off = False
    
    plot_up =   back_padding_vector(up, bot_len- modif_bot_end)
    plot_bot = front_padding_vector(bot, modif_up_st)
    # next update coordinates
    results.append(Aligment_results(mean_squared_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))
    
    max_slider = a.shape[0] + b.shape[0]
    for i in range(max_slider):

        if sub_up.shape[0] != up_len and not sliding_off:
            # step in
            modif_up_st -= 4
            modif_bot_end += 4
            sub_up = up[modif_up_st:modif_up_end]
            sub_bot = bot[modif_bot_st:modif_bot_end]
            
            if sub_up.shape[0] != sub_bot.shape[0] or  sub_up.shape[0] < minim_overlap:
                pass    
            else:
                plot_up = back_padding_vector(up, bot_len- modif_bot_end)
                plot_bot = front_padding_vector(bot, modif_up_st)
                results.append(Aligment_results(mean_squared_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))

            
        elif sub_up.shape[0] == up_len and modif_bot_end != bot_len:
            # slide over
            modif_bot_st += 4
            modif_bot_end += 4
            sub_up = up[modif_up_st:modif_up_end]
            sub_bot = bot[modif_bot_st:modif_bot_end]
                                    
            if sub_up.shape[0] != sub_bot.shape[0] or  sub_up.shape[0] < minim_overlap:
                pass
            else:
                
                plot_up = back_padding_vector(up, bot_len- modif_bot_end)
                plot_up = front_padding_vector(plot_up, modif_bot_st)
                plot_bot = bot 
                results.append(Aligment_results(mean_squared_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))



        else:
            # step out
            sliding_off = True
            modif_bot_st += 4
            modif_up_end -= 4

            sub_up = up[modif_up_st:modif_up_end]
            sub_bot = bot[modif_bot_st:modif_bot_end]
            
            
            if sub_up.shape[0] != sub_bot.shape[0] or  sub_up.shape[0] < minim_overlap:
                break
            else:
                plot_up = front_padding_vector(up, modif_bot_st)
                plot_bot = back_padding_vector(bot, up_len- modif_up_end)
                results.append(Aligment_results(mean_squared_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))
        
#         # MEASURE AND SAVE 
#         # control st ... must be zero
#         if sub_up.shape[0] != sub_bot.shape[0] or  sub_up.shape[0] < 12:
#             pass
#         else:
            
#             if sliding_off:
#                 plot_up = front_padding_vector(up, modif_bot_st)
#                 plot_bot = back_padding_vector(bot, up_len- modif_up_end)
#                 results.append(Aligment_results(mean_absolute_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))
#             else:
#                 if sub_up.shape[0] == up_len and modif_bot_end != bot_len:
#                     print("LOLOLO", bot_len- modif_bot_end, modif_bot_st )
#                     plot_up = back_padding_vector(up, bot_len- modif_bot_end)
#                     plot_up = front_padding_vector(plot_up, modif_bot_st)
#                     plot_bot = bot # front_padding_vector(bot, modif_up_st)
#                     results.append(Aligment_results(mean_absolute_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))
                
#                 else:
#                     plot_up = back_padding_vector(up, bot_len- modif_bot_end)
#                     plot_bot = front_padding_vector(bot, modif_up_st)
#                     results.append(Aligment_results(mean_absolute_error(sub_up,sub_bot),sub_up,  sub_bot, plot_up, plot_bot ))
                
            
    return results


def return_score(values):
    return values.score 

def mse_abs_best(results, n= 10 ):
    
    # sort first
    results.sort(key = return_score )

            
    return results[0:n]

def return_dens_score(values):
    return values.score / values.up_matched.shape[0]

def mse_dens_best(results, n= 10 ):
    
    # sort first
    results.sort(key = return_dens_score,)

            
    return results[0:n]

def pick_best(results, minim_overlap=12):
    maxim = -10
    hit = Aligment_results(-1,[0,0,0,0],[0,0,0,0], [0,0,0,0], [0,0,0,0])
    
    for i in results:
        if i.score > maxim and i.up_matched.shape[0] >= minim_overlap:
            maxim = i.score
            
            hit = i
            
    return hit

def full_comparision(buffer_pred, longest, minim_overlap=12):
    
    buffer_pred_reversed = revcom_vector(buffer_pred)
    
    if buffer_pred.shape[0] == longest.values.flatten().shape[0]:
        
        
        score = compare_motifs(buffer_pred, longest.values.flatten().astype(float))
        rev_score = compare_motifs(buffer_pred_reversed, longest.values.flatten().astype(float))
        if score > rev_score:
            
            return score, buffer_pred, longest
        else:
            #buffer_pred = buffer_pred_reversed
            #correlation_scores_cnn.append(rev_score)
            return rev_score, buffer_pred_reversed, longest
    
    
    else:
        
        all_slides = slide_over_mse(buffer_pred, longest.values.flatten().astype(float), minim_overlap=minim_overlap)
        top_slides = mse_dens_best(all_slides, n=4)
        rescore = list()
        for slide in top_slides:
            rescore.append(Aligment_results(compare_motifs(slide.up_matched,slide.bot_matched), slide.up_matched,slide.bot_matched,slide.up_plot,slide.bot_plot) )
        
        forward = pick_best(rescore, minim_overlap )

        
        all_slides = slide_over_mse(buffer_pred_reversed, longest.values.flatten().astype(float), minim_overlap=minim_overlap)
        top_slides = mse_dens_best(all_slides, n=4)
        #for slide in top_slides:
        rescore = list()
        for slide in top_slides:
            rescore.append(Aligment_results(compare_motifs(slide.up_matched,slide.bot_matched), slide.up_matched,slide.bot_matched,slide.up_plot,slide.bot_plot) )
        
        reverse = pick_best(rescore, minim_overlap)
                                   
         
        if buffer_pred.shape[0] > longest.values.flatten().shape[0]:
            
            if forward.score > reverse.score:
                return forward.score, forward.up_plot, forward.bot_plot
            else:
            #buffer_pred = buffer_pred_reversed
            #correlation_scores_cnn.append(rev_score)
                return reverse.score, reverse.up_plot, reverse.bot_plot
        
        
        
        else:
            if forward.score > reverse.score:
                return forward.score,  forward.bot_plot, forward.up_plot
            else:
            #buffer_pred = buffer_pred_reversed
            #correlation_scores_cnn.append(rev_score)
                return reverse.score,  reverse.bot_plot, reverse.up_plot


def fast_full_comparision(buffer_pred, longest, minim_overlap=12):
    
    buffer_pred_reversed = revcom_vector(buffer_pred)
    
    if buffer_pred.shape[0] == longest.values.flatten().shape[0]:
        
        
        score = compare_motifs(buffer_pred, longest.values.flatten().astype(float))
        rev_score = compare_motifs(buffer_pred_reversed, longest.values.flatten().astype(float))
        if score > rev_score:
            
            return score, buffer_pred, longest
        else:
            #buffer_pred = buffer_pred_reversed
            #correlation_scores_cnn.append(rev_score)
            return rev_score, buffer_pred_reversed, longest
    
    
    else:
        
        all_slides = slide_over_mse(buffer_pred, longest.values.flatten().astype(float), minim_overlap=minim_overlap)
        forward = mse_dens_best(all_slides, n=1)[0]

        all_slides = slide_over_mse(buffer_pred_reversed, longest.values.flatten().astype(float), minim_overlap=minim_overlap)
        reverse = mse_dens_best(all_slides, n=1)[0]

                                            
        if buffer_pred.shape[0] > longest.values.flatten().shape[0]:
            
            if forward.score > reverse.score:
                return forward.score, forward.up_plot, forward.bot_plot
            else:
            #buffer_pred = buffer_pred_reversed
            #correlation_scores_cnn.append(rev_score)
                return reverse.score, reverse.up_plot, reverse.bot_plot
                
        else:
            if forward.score > reverse.score:
                return forward.score,  forward.bot_plot, forward.up_plot
            else:
            #buffer_pred = buffer_pred_reversed
            #correlation_scores_cnn.append(rev_score)
                return reverse.score,  reverse.bot_plot, reverse.up_plot,



def viz_motifaln(results, alnid):
    for x in results:
        if x[0] == alnid:
            #print('stop')
            
        
        
            forlogo_pred_rf = two_bits(vec2df(x[4]))
            forlogo_pred_rf.fillna(.0, inplace=True)
            logomaker.Logo(forlogo_pred_rf.astype(float))   

            forlogo_pred_rf = two_bits(vec2df(x[5]))
            forlogo_pred_rf.fillna(.0, inplace=True)

            logomaker.Logo(forlogo_pred_rf.astype(float))
            break
    
    return