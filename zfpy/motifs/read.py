import sys
import json
import os
import pickle
import random
import argparse
import math
import re
from collections import defaultdict, namedtuple
import numpy as np
import pandas as pd
import glob


from multiprocessing import Pool
from functools import partial

from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr
from scipy.stats import entropy


from  ..base import *
from ._base import *



def read_bulk_pwms(pwm_path, motif_tag='TF\t'):
    PWMs = dict()
    with open(pwm_path,'r') as fn:
        read_flag = False
        for l in fn:
            #if len(l[0])==0:
            #    print('SSSS')
            #print(len(l), l)
            
            if l.startswith(motif_tag):
                
                name, tf_name, gene, motif = '','','', ''
                data_tosave = list() 
                read_flag = True
            
            if l.startswith('TF\t'):

                name = l.strip().split('\t')[1]
                continue
                

            if l.startswith('TF Name\t'):
                tf_name = l.strip().split('\t')[1]
                continue
                
                
            if l.startswith('Gene\t'):
                gene = l.strip().split('\t')[1]
                continue
                
            if l.startswith('Motif\t'):
                motif = l.strip().split('\t')[1]
                continue
                

                
                
            if read_flag and re.match('\d',l[0]):
                buffer = l.strip().split('\t')[1:]
                buffer = [float(x) for x in buffer]
                data_tosave.append(buffer)
                continue
                

            if read_flag and len(l) == 1:
                #swifht off flag
                read_flag = False
                #save data
                PWMs['{}|{}|{}|{}'.format(name, tf_name, gene, motif)] = pd.DataFrame(data_tosave, columns=['A', 'C', 'T', 'G'])
                continue
                
                
    return PWMs


# read Marcus PWMS

def read_marcus_pwm(file):
    pwm = pd.read_csv(file, delim_whitespace=True)
    del pwm['0']
    pwm = pwm.T
    pwm.rename(columns={'A:':'A','C:':'C','T:':'T','G:':'G'}, inplace=True)
    return pwm


# read ZifRC's Outputs

def readRFout(path):
    arrays = list()
    matrix = list()
    arr_dx = 0
    with open(path,'r') as fn:
        for line in fn:
            #print(line)
            if line.startswith('Pos') and arr_dx != 0:
                    a = pd.DataFrame(matrix, index=list(range(len(matrix))),columns=('A','C','G','T')  )
                    arrays.append(a)
                    #print('')
                    matrix = list()
                    arr_dx += 1
            if line.startswith('Pos') and arr_dx == 0:
                arr_dx += 1
                
            if re.findall('^\d',line):
                    
                matrix.append(line.split()[1:])
                        
    a = pd.DataFrame(matrix, index=list(range(len(matrix) ) ),columns=('A','C','G','T')  )
    arrays.append(a)

    return  arrays


def find_target_file(row):

    all_files = glob.glob('/home/ccorbi/Work/Beagle/Short_Projects/ZFC/ZifRC/out/*')


    pat = 'D.......{}.{}{}..{}H...H.'
          #DICGRKFSYSHHLSRHTKIHLRQKD
    

    for i in all_files:
        hit = re.findall(pat.format(row[0],row[1],row[2],row[3]), i)
        if hit:
            #print(hit)
            a = readRFout(i+'/results.PFM.txt')
            if a[0].empty:
                continue
            else:
                return a[0]
            #print(a)
    return pd.DataFrame()
