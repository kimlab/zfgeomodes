import numpy as np
import pandas as pd

from scipy.stats import entropy


from ..base import *



def vec2df(vector):
    '''
    Convert flat vector to a DataFrame.
    
    Args:
        vector (ndarray): 1D-array with the values
        
    Returns:
        DataFrame: A dataframe with the information content per postion
        
    '''
    
    l = int(len(vector) / 4.0)
    matrix = list()
    for i in range(0,len(vector),4):
        matrix.append(vector[i:i+4])
    m =  pd.DataFrame(matrix, index=list(range(l)),columns=( 'A','C' ,'G', 'T')  )


    return m

def df2vec():
    
    return

def squeez(fingers, features=features):
    '''
    Extract Base values from Dataframe
    
    Args:
        df (DataFrame): the frequency matrix in Dataframe
    
    Returns:
        ndarray: An array with the values
    '''


    matrix = fingers[features].values.flatten().astype(float)

    return matrix



def pwm2freq(vector):
    '''
    Convert log-odds to frequency.
    
    Args:
        vector (ndarray): 1D-array with the pwm
        
    Returns:
        DataFrame: A dataframe with the information content per postion
    '''

    convert = lambda x: math.pow(2,x) /4.0
    
    m = vec2df(vector)
    m = m.applymap(convert)


    return m

def mrange(vector):
    '''
    Max range. finds the largest delta within the array. 
    Helping function for score2freq function
    
    
    Args:
        vector (ndarray): 1D-array with the zscores
        
    Returns:
        float: maximum detla within the vector
        
    '''  
    deltas = list()
    for i in range(0,len(vector),4):
        v=vector[i:i+4]
        deltas.append(np.max(v) - np.min(v))
        
    return max(deltas)

def score2freq(vector):
    '''
    Converts Zscore values to frequencies. Zscore from B1H reads. 
    Using method described in the paper Najafabadi 2015.
    
    
    Args:
        vector (ndarray): 1D-array with the zscores
        
    Returns:
        ndarray: An array with the frequency content per postion
        
    '''    
    c = mrange(vector) / np.log(50)
    c += 0.00000001
    b = [math.exp(x/c) for x in vector]
    #print(b)
    matrix = list()
    for i in range(0,len(b),4):
        sum_exp = sum(b[i:i+4])
        onepos = np.asarray(b[i:i+4])
        onepos = onepos / sum_exp
        matrix.extend(onepos)
    return np.asarray(matrix)


def two_bits(df):
    '''
    AKA freq2bits. Converts log-odds to bits. This function is specific 
    for this project, for more generic function, use ngskit.
    
    
    Args:
        df (DataFrame): the frequency matrix in Dataframe
        
    Returns:
        DataFrame: A dataframe with the information content per postion
        
    '''
    # each row is a position
    new_matrix  = list()
    for idx, row in df.iterrows():
        h = entropy(row.values, base=2)
        # no correction
        en = 0
        r = np.log2(4) - h
        temp = row.values*r
        new_matrix.append(temp.tolist())
        #new_matrix = np.asarray(new_matrix)
    new_matrix = pd.DataFrame(new_matrix, columns=['A', 'C', 'G', 'T'])
    return new_matrix


def revcom_vector(vec):
    #m = vec2df(vec)
    #rc = reverse_comp(m)
    
    return vec[::-1]


def norm_output(b):
    '''
    Normalization of values of the NN output. 
    All values for position add to 1. (Probabiltiy)
    
    
    Args:
        vector (ndarray): 1D-array with the NN outputs
        
    Returns:
        ndarray: 1D-array with the frequency 
        
    '''        
    result = list()
    for i in [0,4,8]:
        #print(b[:,i:i+4])
        a = b[:,i:i+4].sum()
        #print(a)
        result.append(b[:,i:i+4] / a)
    return np.asarray(result).flatten()