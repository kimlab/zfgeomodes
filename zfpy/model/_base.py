import sys
import json
import os
import pickle
import random
import argparse
import math
import re
from collections import defaultdict
import numpy as np
import pandas as pd
import json


import joblib

import matplotlib
import matplotlib.pyplot as plt

import keras.backend as K
from keras import backend as K
import tensorflow as tf

from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau, CSVLogger, ModelCheckpoint
from keras.initializers import RandomNormal
from keras.layers import Dense, LSTM, GRU, Dropout, Embedding
from keras.models import Sequential, load_model, Model
from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D, MaxPooling2D, Convolution2D, Convolution1D
from keras.layers import Conv1D,  UpSampling1D, MaxPooling1D, LSTM, Dropout, TimeDistributed
from keras.layers import LeakyReLU, Dropout, GlobalMaxPooling1D, Input, Concatenate
from keras.layers import Dense, Dropout, Flatten, Activation, BatchNormalization, regularizers, Reshape
from keras.layers import InputLayer,  regularizers

from keras.optimizers import Adam, SGD, RMSprop

import keras.losses
import keras.metrics


from sklearn.utils import shuffle
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error

from scipy.spatial import distance
from scipy.stats import pearsonr, spearmanr, entropy




#config = tf.ConfigProto()
#config.gpu_options.allow_growth = True
#sess = tf.Session(config=config)



def correlation_coefficient_loss(y_true, y_pred):
    x = y_true
    y = y_pred
    mx = K.mean(x)
    my = K.mean(y)
    xm, ym = x-mx, y-my
    r_num = K.sum(tf.multiply(xm,ym))
    r_den = K.sqrt(tf.multiply(K.sum(K.square(xm)), K.sum(K.square(ym))))
    r = r_num / r_den

    r = K.maximum(K.minimum(r, 1.0), -1.0)
    return 1 - K.square(r)



keras.losses.correlation_coefficient_loss = correlation_coefficient_loss
keras.metrics.correlation_coefficient_loss = correlation_coefficient_loss

models_dir = os.path.dirname(__file__) #<-- absolute dir the script is in



def load_pretrained_models(train_on='Hamed', input_features='Query', ml='SVR'):

    target_model = '{}_{}_{}'.format(train_on, ml,input_features)

    models_index = {'Dmel_SVR_Query':'Dmel_SVR_Query.pkl',
                    'Dmel_SVR_Helix':'Dmel_SVR_Helix.pkl',
                    'Dmel_SVR_Core':'Dmel_SVR_Core.pkl',
                    'Hamed_SVR_Query':'Hamed_SVR_Query.pkl',
                    'Hamed_SVR_Helix':'Hamed_SVR_Helix.pkl',
                    'Hamed_SVR_Core':'Hamed_SVR_Core.pkl',
                    'dmel_RF_Query':'dmel_RF_Query.pkl',
                    'dmel_RF_Helix':'dmel_RF_Helix.pkl',
                    'dmel_RF_Core':'dmel_RF_Core.pkl',
                    'Hamed_RF_Query':'Hamed_RF_Query.pkl',
                    'Hamed_RF_Helix':'Hamed_RF_Helix.pkl',
                    'Hamed_RF_Core':'Hamed_RF_Core.pkl',
                    'Noyes_RF_Query':'201912_RF_Query.pkl',
                    }

    relpath = './pretrained_models/{}'.format(models_index[target_model])
    abs_file_path = os.path.join(models_dir, relpath)

    if not os.path.exists(abs_file_path):
        return FileNotFoundError
    
    elif ml == 'SVR':

        return pickle.load(open(abs_file_path, 'rb'))
    
    elif ml == 'RF':

        return joblib.load(open(abs_file_path, 'rb'))

    else:
        return KeyError


def modelbundle_pred(modelbundle, predx,  input_features='Query') :

    size_vector = {'Query':168,'Helix':126,'Core':84}
    pred_ = list()
    for i in range(12):
        f = np.reshape(predx, (1,size_vector[input_features]))
        pred_.append(modelbundle[i].predict(f))
        pred = np.asarray(pred_)
    pred = np.asarray(pred_)

    return pred



def modelbundle_eval(modelbundle, testx, testy, input_features='Query') :

    results = list()
    for idx, finger in enumerate(testx): 
        
        pred = modelbundle_pred(modelbundle,finger, input_features)
        results.append(mean_squared_error(pred, testy[idx]))
        
    print(np.median(results))
    return results


