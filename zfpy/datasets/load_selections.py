import re
import os
import pandas as pd

from ..base import *




def extract_6(zfinger):
    parts = []
    if len(zfinger) !=0:
        for i in  [8,9,10,11,13,14]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None


def extract_4(zfinger):
    parts = []
    if len(zfinger) !=0:

        for i in  [8,10,11,14]:
            parts.append(zfinger[i:i+1])
        return ''.join(parts)
    return None

def load_experimental_2019():
    
    
    collector = list()
    modes = [ 'MODE6',  'RS', 'MODE4', 'MODE3','MODE5', 'MODE2', 'RA',]
    bres = {'RS':['RS'],'RA':['RA'],
                        'MODE2':['RR'],'MODE3':['VS'],
                        'MODE4':['KS'], 'MODE5':['IN'],'MODE6':['KR'], 'OVERLAP': ['RA']}



    #modes = [ 'MODE6', , 'MODE3','MODE5', 'MODE2','RA', 'RS' ]

    for o in modes:

        for t in  triplets:
            try:
                #m = overlaping[o]
                #p = prefix[o]
                df = pd.read_csv(f'/home/kimlab2/ccorbi/MARCUS_ZF/preprocess_2019/{o}_{t}_preprocessi_2019.csv')
                df['Triplet'] = t
                df['Quad'] = t + 'A'
                df['Overlap'] = 'A'
                df['nReads'] =  (df['Reads']/df['Reads'].sum()) * 100_000
                df['eReads'] =  np.log2(df['Reads']/df['Reads'].mean())
                df['BP'] = bres[o][0]

                df['OL_vec'] = 'X'
                df = df[df['Seq'].str.contains(r'DICGRKF{}....L..HTKIHLRQKD'.format(bres[o][0][1]))]

                collector.append(df)
            except Exception as e:
                print('Something went wrong for {} {}'.format(t, o))
                #print(e)
                pass
            
    mergeddf = pd.concat(collector)
    mergeddf['Core'] =mergeddf['Seq'].apply(extract_4)
    mergeddf['Helix'] =mergeddf['Seq'].apply(extract_6)

    return mergeddf
    
def load_experimental_complete():
    
    
    collector = list()
    modes = [ 'MODE6',  'RS', 'MODE4', 'MODE3','MODE5', 'MODE2', 'RA',]
    bres = {'RS':['RS'],'RA':['RA'],
                        'MODE2':['RR'],'MODE3':['VS'],
                        'MODE4':['KS'], 'MODE5':['IN'],'MODE6':['KR'], 'OVERLAP': ['RA']}



    #modes = [ 'MODE6', , 'MODE3','MODE5', 'MODE2','RA', 'RS' ]

    for o in modes:

        for t in  triplets:
            try:
                #m = overlaping[o]
                #p = prefix[o]
                df = pd.read_csv(f'/home/kimlab2/ccorbi/MARCUS_ZF/preprocess_complete/{o}_{t}_preprocess.csv')
                df['Triplet'] = t
                df['Quad'] = t + 'A'
                df['Overlap'] = 'A'
                df['nReads'] =  (df['Reads']/df['Reads'].sum()) * 100_000
                df['eReads'] =  np.log2(df['Reads']/df['Reads'].mean())
                df['BP'] = bres[o][0]

                df['OL_vec'] = 'X'
                df = df[df['Seq'].str.contains(r'DICGRKF{}....L..HTKIHLRQKD'.format(bres[o][0][1]))]

                collector.append(df)
            except Exception as e:
                print('Something went wrong for {} {}'.format(t, o))
                #print(e)
                pass
            
    mergeddf = pd.concat(collector)
    mergeddf['Core'] =mergeddf['Seq'].apply(extract_4)
    mergeddf['Helix'] =mergeddf['Seq'].apply(extract_6)

    return mergeddf
    
def load_experimental_OL_2019():
    
    
    collector = list()
    overlaping = {'NNNaAC-3':'A', 'NNNcAG-1b':'C', 'NNNgAC-3b':'G'}



    prefix = {'NNNcAG-1b':'3ZF1b', 'NNNgAC-3b':'3ZF3b_correct'  , 'NNNaAC-3': '3ZF3correct' }
    for o in overlaping:
        for t in  triplets:
            try:
                m = overlaping[o]
                p = prefix[o]
                df = pd.read_csv(f'/home/kimlab2/ccorbi/MARCUS_OVERLAp/{o}/preprocess_2019/{p}_{t}_preprocess.csv')
                df['Triplet'] = t
                df['Quad'] = t + m
                df['Overlap'] = m
                df['BP'] = 'RA'
                df['nReads'] =  (df['Reads']/df['Reads'].sum()) * 100_000
                df = df[df['Seq'].str.contains(r'DICGRKFA....L..HTKIHLRQKD')]

                collector.append(df)
            except:
                print('Something went wrong for {} {}'.format(t, o))
                pass

            
    mergeddf = pd.concat(collector)
    
    
    mergeddf['Core'] =mergeddf['Seq'].apply(extract_4)
    mergeddf['Helix'] =mergeddf['Seq'].apply(extract_6)
    return mergeddf
    

def load_experimental_OL_complete():
    
    
    collector = list()
    overlaping = {'NNNaAC-3':'A', 'NNNcAG-1b':'C', 'NNNgAC-3b':'G'}



    prefix = {'NNNcAG-1b':'3ZF1b', 'NNNgAC-3b':'3ZF3b_correct'  , 'NNNaAC-3': '3ZF3correct' }
    for o in overlaping:
        for t in  triplets:
            try:
                m = overlaping[o]
                p = prefix[o]
                df = pd.read_csv(f'/home/kimlab2/ccorbi/MARCUS_OVERLAp/{o}/preprocess_complete/{p}_{t}_preprocess.csv')
                df['Triplet'] = t
                df['Quad'] = t + m
                df['Overlap'] = m
                df['BP'] = 'RA'
                df['nReads'] =  (df['Reads']/df['Reads'].sum()) * 100_000
                df = df[df['Seq'].str.contains(r'DICGRKFA....L..HTKIHLRQKD')]

                collector.append(df)
            except:
                print('Something went wrong for {} {}'.format(t, o))
                pass

            
    mergeddf = pd.concat(collector)
    
    
    mergeddf['Core'] =mergeddf['Seq'].apply(extract_4)
    mergeddf['Helix'] =mergeddf['Seq'].apply(extract_6)
    return mergeddf


def show_helix(seq, modes, triplet,  dataset, show_core=False):
    helix = seq[0:4] + seq[5:7]  
    core = seq[0] + seq[2:4] +seq[-1]
    assert len(helix) == 6
    assert len(core) == 4
    
    for m in modes:
        
        if show_core:
        
            df = dataset[(dataset['core']==core)&(dataset['MODE']==m)&
                    (dataset['Triplet']==triplet)]

            if df.empty:
                reads = 0
            else:
                reads =  df['Reads'].sum()
            print('{}\t{}\t{}\t{}'.format(core, m ,triplet, reads ))

        else:
            
            df = dataset[(dataset['helix']==helix)&(dataset['MODE']==m)&
                        (dataset['Triplet']==triplet)]
            if df.empty:
                reads = 0

            else:
                assert  df['Reads'].sum() == df['Reads'].values[0]
                reads = df['Reads'].sum()
                
            print('{}\t{}\t{}\t{}'.format(helix, m ,triplet, reads ))