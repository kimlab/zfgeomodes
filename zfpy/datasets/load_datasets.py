
import sys
import os
import pickle
import re
import numpy as np
import pandas as pd


from ..base import *
from ..motifs import squeez, score2freq
from ..motifs.read import read_bulk_pwms, read_marcus_pwm
from .load_cisbp import *



script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in



def set_vector_size(encode_field):
    
    if encode_field in ['Query', 'Helix', 'Core']:
        pass
    else:
        raise ValueError
        
    if encode_field == 'Query':
        vector_size = 8
    elif encode_field == 'Helix':
        vector_size = 6    
    else:
        # Core
        vector_size = 4
        
    return vector_size



## Code for parsing T.Hugues 2015

def _extract_helix(s):
    specres = list()

    core = re.findall(r'C.{2,5}C.{3}[LIVMFYWC].{8}H', s)
    if core:
        #canonical

        for i in [-8,-7,-6,-5,-3,-2]:
            specres.append(core[0][i])
        
        return ''.join(specres)

    core = re.findall(r'C.{2,5}C.{12}H', s)
    if core:
        for i in [-8,-7,-6,-5,-3,-2]:
            specres.append(core[0][i])
        
        return ''.join(specres)


    return ''.join(['X','X','X','X','X','X'])

def _extractH_mode(s):
    core = re.findall(r'C.{2,5}C.{3}[LIVMFYWC].{8}H', s)
    if core:

        mplus2 = core[0][-9]
        
        return 'R{}'.format(mplus2)

    core = re.findall(r'C.{2,5}C.{12}H', s)
    if core:

        mplus2 = core[0][-9]
        return 'R{}'.format(mplus2)


    return ''.join(['X','X','X','X','X','X'])




    
def load_hughes_tf_repertoire_2015():
    relpath = './data/B1HLib_Hughes.2015.csv'
    abs_file_path = os.path.join(script_dir, relpath)
    df = pd.read_csv(abs_file_path)
    df['Helix'] = df['ZF protein sequence'].apply(_extract_helix)
    df['Core'] = df['Helix'].str[0] + df['Helix'].str[2:4] + df['Helix'].str[-1]

    return df


def load_zifRC_training_index():
    
    relpath = './data/ZifRC_Hughes.2015.csv'
    abs_file_path = os.path.join(script_dir, relpath)

    b1h_hamed = pd.read_csv(abs_file_path)
    b1h_hamed['Helix'] = b1h_hamed['ZF_sequence'].apply(_extract_helix)
    b1h_hamed['BP'] = b1h_hamed['ZF_sequence'].apply(_extractH_mode)
    b1h_hamed['Core'] = b1h_hamed['Helix'].str[0] + b1h_hamed['Helix'].str[2:4] +b1h_hamed['Helix'].str[-1]   
    b1h_hamed['Query'] = b1h_hamed['BP'] + b1h_hamed['Helix'] 
    
    return b1h_hamed


def load_zifRC_training_dataset(encode_field = 'Query', features=features):
    
    df = load_zifRC_training_index()
    df = df[~df['Helix'].str.contains('X')]

    
    gsd_xtest = list()
    gsd_ytest = list()
    
    for idx, finger in df.iterrows():
        
        seq = finger[encode_field]
        query = onehot_seq(seq, kind = 'aa')


        target = score2freq(finger[features])
        gsd_xtest.append(query)
        gsd_ytest.append(target.flatten().astype(float))
        
    gsd_ytest = np.asarray(gsd_ytest)
    gsd_xtest = np.asarray(gsd_xtest)

    return gsd_xtest, gsd_ytest  


## Load Noyes Datasets

def load_F2data():

    foldx = pickle.load(open('./data/F2_Xtotal_nr_clean.pkl', 'rb'))
    y = pickle.load(open('./data/F2_ytotal_nr_clean.pkl', 'rb'))

    expanded = list()
    for X in foldx:
            extra = onehot_seq('RA')
            temp = np.reshape(X,(6,21))
            temp = np.concatenate([extra, temp])
            temp = np.reshape(temp,(8,21))
            expanded.append(temp)
    x = np.asarray(expanded)

    return x, y


def load_F3data():

    foldx = pickle.load(open('./data/F3_Xtotal_nr_clean.pkl', 'rb'))
    y = pickle.load(open('./data/F3_ytotal_nr_clean.pkl', 'rb'))

    expanded = list()
    for X in foldx:
            extra = onehot_seq('RS')
            temp = np.reshape(X,(6,21))
            temp = np.concatenate([extra, temp])
            temp = np.reshape(temp,(8,21))
            expanded.append(temp)
    x = np.asarray(expanded)

    return x, y


# load Dmel Scott dataset

def load_dmel_index():
    rel_path = './data/Dmel_Wolfe.2013.csv'
    abs_file_path = os.path.join(script_dir, rel_path)
    GSgs2014dm = pd.read_csv(abs_file_path , keep_default_na=False)
    GSgs2014dm.rename(columns={'SRES':'Helix', 'MODE':'BP'}, inplace= True)
    GSgs2014dm['Core'] = GSgs2014dm['Helix'].str[0] + GSgs2014dm['Helix'].str[2:4] + GSgs2014dm['Helix'].str[-1]
    GSgs2014dm['BP'].fillna('__', inplace=True)
    GSgs2014dm['Query'] = GSgs2014dm['BP'] + GSgs2014dm['Helix']
    return GSgs2014dm

def load_dmel_dataset(encode_field='Query'):
    GSgs2014dm = load_dmel_index()

    #vector_size = set_vector_size(encode_field)
            
    gsd_xtest = list()
    gsd_ytest = list()

    for idx, tfn in GSgs2014dm.groupby('TFid', as_index=False):

        tfn.reset_index(drop=True, inplace=True)
        for idj, finger in tfn.iterrows():
            seq = finger[encode_field]
            query = onehot_seq(seq, kind = 'aa')
            #query = np.reshape(query,(vector_size,21))
            targ = squeez(finger)
            gsd_xtest.append(query)
            gsd_ytest.append(targ)

    gsd_ytest = np.asarray(gsd_ytest)
    gsd_xtest = np.asarray(gsd_xtest)

    return gsd_xtest, gsd_ytest
    

#load 100 validated

def load_100_index():

    rel_path = './data/B1H100_Noyes.2019/100_pwm_index.csv'
    abs_file_path = os.path.join(script_dir, rel_path)
    df = pd.read_csv(abs_file_path)    
    
    return df

def load_100_dataset(encode_field = 'Query'):

    rel_path = './data/B1H100_Noyes.2019/PWM/'
    abs_file_path = os.path.join(script_dir, rel_path)

    filebasepath =  abs_file_path + '/{}.txt'
    
    df = load_100_index()

    gsd_xtest = list()
    gsd_ytest = list()
    
    for idx, finger in df.iterrows():
        
        seq = finger[encode_field]
        query = onehot_seq(seq, kind = 'aa')

        target = read_marcus_pwm(filebasepath.format(finger['pwm_name']))


        gsd_xtest.append(query)
        gsd_ytest.append(target.values.flatten())
        
    gsd_ytest = np.asarray(gsd_ytest)
    gsd_xtest = np.asarray(gsd_xtest)

    return gsd_xtest, gsd_ytest   
            


def load_cele(encoded=True):

    rel_path = './data/CelegansPBM_Hughes.2015/'
    abs_file_path = os.path.join(script_dir, rel_path)

    
    cele = load_cisbp_tfome(f'{abs_file_path}/prot_seq.txt')
    if encoded:
        motifs = read_bulk_pwms(f'{abs_file_path}/PWM.txt', motif_tag='Gene\t')
        #return motifs 
        for header, matrix in motifs.items():
                df = cele[cele['TF_Name']==header.split('|')[2]]
                if df.empty:
                    print(header, header.split('|'))
                    continue
                else:
                    
                    pass
                
    else:
        return cele
