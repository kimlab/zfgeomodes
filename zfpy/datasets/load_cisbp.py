
import sys
import os
import pickle
import re
import numpy as np
import pandas as pd

from ..base import *


def _extract_mode_nine(s):

    ct = re.findall(r'C.{3}[LIVMFYWC].{8}H..', s)
    if ct:
        return ct[0][-1]

    # no canoncial
    ct = re.findall(r'C.{2,5}C.{12}H..', s)

    if ct:
        return ct[0][-1]

    return '_'

def _extract_specaminoacids(s):
    specres = list()

    core = re.findall(r'C.{2,5}C.{3}[LIVMFYWC].{8}H..', s)
    if core:
        #canonical

        for i in [-10,-9,-8,-7,-5,-4]:
            specres.append(core[0][i])
        mplus2 = core[0][-11]
        return ''.join(specres), mplus2, 'CANONICAL'

    core = re.findall(r'C.{2,5}C.{12}H..', s)
    if core:
        for i in [-10,-9,-8,-7,-5,-4]:
            specres.append(core[0][i])
        mplus2 = core[0][-11]
        return ''.join(specres), mplus2, 'canonical'        


    return ''.join(['X','X','X','X','X','X']), 'X', 'NOTFOUND'


def load_cisbp_tfome(tfome_path='/home/ccorbi/Work/8-Short-projects/ZincFingers_class/deepzf/Datasets/external/cisBP_C2H2v2.00/prot_seq.txt'):
    
    tfome = pd.read_csv(tfome_path, sep='\t')
    results = list()
    
    for idx, tf in tfome.iterrows():
        Pfam_froms = tf['Pfam_froms'].split(',')
        Pfam_tos = tf['Pfam_tos'].split(',')
        DBD_seqs = tf['DBD_seqs'].split(',')
        collected = list()
        m0 = '_'
        BP = ''
        prev = 0
        array = 1
        for idx, finger in enumerate(DBD_seqs):
            temp = list()
            specres, m1, ztype = _extract_specaminoacids(finger)

            if ztype == 'NOTFOUND':
                continue

            temp.append(tf['Protein_ID'])
            temp.append(tf['TF_ID'])
            temp.append(tf['TF_Name'])
            temp.append(tf['Gene_ID'])
            temp.append(tf['Species'])

            temp.append('F{}'.format(idx+1))

                # identify Array 

            if prev == 0:

                #array =
                temp.append(array)
                temp.append(m0+m1)
                temp.append(specres)
                temp.append(Pfam_froms[idx])
                temp.append(Pfam_tos[idx])
                temp.append(finger)
                m0 = _extract_mode_nine(finger)
                prev = int(Pfam_tos[idx])



            elif int(Pfam_froms[idx])-prev >8:

                array = array + 1

                temp.append(array)
                m0 = '_'
                temp.append(m0+m1)
                temp.append(specres)
                temp.append(Pfam_froms[idx])
                temp.append(Pfam_tos[idx])
                temp.append(finger)
                m0 = _extract_mode_nine(finger)
                prev = int(Pfam_tos[idx])



            else:

                #array =
                temp.append(array)
                temp.append(m0+m1)
                temp.append(specres)
                temp.append(Pfam_froms[idx])
                temp.append(Pfam_tos[idx])
                temp.append(finger)
                m0 = _extract_mode_nine(finger)
                prev = int(Pfam_tos[idx])

        #print(collected)
            results.append(temp)
        #results.append(collected)


    results = pd.DataFrame(results, columns=['Protein_ID','TF_ID', 'TF_Name', 'Gene_ID', 'Species','Finger', 'Array', 'BP', 'Helix','from','end', 'Seq' ])

    results = results.drop_duplicates(subset=[ 'Gene_ID', 'Species', 'Finger',
       'Array', 'BP', 'Helix',  'Seq'])
    
    results['Core'] = results['Helix'].str[0] + results['Helix'].str[2:4] +results['Helix'].str[-1]

    
    return results

