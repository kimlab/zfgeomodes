# ZF Geometric Modes

This repository contains the code and data of the manuscript **The geometric influence on the Cys2His2 zinc finger domain and interaction plasticity**. For a full explanation of the data repository, see the folders description section below. 
The prediction tool **ZFpred** is part of the package **ZFpy**. The latter contains all methods, classes and functions used to generate the analysis of the manuscript. 

## Installation

Optional, but highly recommended, install the package within a virtual environment (Virtualenv or a Conda)

1. Install git-lfs: `conda install -c conda-forge git-lfs && git lfs install`
2. Clone this repository: ` git clone https://gitlab.com/kimlab/zfgeomodes.git`
3. install requirements: `pip install  -r requirements.txt`
4. install package: `pip install  .`


## ZFpred

ZFpred is a utility within ZFpy to generate predictiona with a pre-trained model. ZFpred input is a Fasta file, containing one or multiple transcription factors. The code will extract the ZF, and for each array will generate a DNA binding motifs. The prediction will be written in a text file, and a logo will be plotted in pdf format. 

```bash
cd examples
python ../zfpy/zfpred.py -i zfpred_example.fasta
```


## Folders content description


```
├── examples/
    [example files, and Jupyter notebooks with a few examples]

├── data/
    [data repo and some supplementary material  of the ZFpy project]
	└── MD_clusters/
      [PDB files of the conformational clusters identified on the molecular dynamics simulations]
	└── modes_PDBs/
      [PDB files of the modes identified by M. Garton et al. [PMID:26384429]]
	└── preprocess/
      [Preprocessed data from the B1H screenings, clustering, etc.]
  └── manuscript_tables/
      [main and supplemental tables published in the manuscript]

├──  zfpy/ 
     [Python package containing all the code used in the manuscript and ZFpred model to generate a DNA binding motif prediction]
	└── datasets/
      [validation datasets and code for data loading and parsing]
	└── motifs/
      [code for motifs manipulation and plotting]
	└── utils/
      [a miscellanea box of functions with limited utility]
	└── model/
      [Code and pre-trained models]
	└── pipelines/
        	└── preprocessing/
              [Scripts for preprocessing data]
       		└── hyperparm_search/
              [Scripts for the random search of hyperparameters] 

```


## Requirements

  - pandas
  - numpy
  - keras
  - scipy
  - pytest
  - scikit-learn
  - tqdm
  - python-Levenshtein
  - logomaker
  - matplotlib
  - ngskit (optional, only for the preprocessing pipeline) [here](https://gitlab.com/kimlab/ngskit)
  - tensorflow
