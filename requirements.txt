pandas<=0.24.2
numpy<=1.16.2
scipy<=1.3.0
pytest
scikit-learn<=0.21.2
tqdm<=4.32.1
python-Levenshtein
matplotlib
seaborn
keras<=2.2.4
logomaker<=0.8 
joblib<=0.14.1
tensorflow<=1.5
